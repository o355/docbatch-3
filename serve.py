#    DOCBatch 3
#    Copyright (C) 2021  Owen McGinley
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, render_template

app = Flask(__name__)


@app.route("/", methods=['GET'])
def main():
    return render_template("base.html")


@app.route("/app", methods=['GET'])
def main_live():
    return render_template("base_live.html")


if __name__ == "__main__":
    app.run(debug=True)