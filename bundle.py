#    DOCBatch 3
#    Copyright (C) 2021  Owen McGinley
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# These locations of files may be different for you.
bundlecontents = ["static/js/console.js", "static/js/Events.js", "static/js/UIController.js", "static/js/Authentication.js",
                  "static/js/Locations.js", "static/js/FormUIController.js", "static/js/DateUI.js", "static/js/ReoccurUI.js",
                  "static/js/TimeUI.js", "static/js/ConfirmUI.js", "static/js/LocationUI.js", "static/js/SubmissionUI.js",
                  "static/js/Reservations.js", "static/js/Reservation.js", "static/js/DeleteReservation.js",
                  "static/js/onload.js", "static/js/EatNow.js", "static/js/uuid4.js", "static/js/Appearance.js"]

outfile = "static/js/bundle-live.js"

with open(outfile, "w") as f:
    f.write("")

for file in bundlecontents:
    with open(file) as f:
        lines = f.readlines()
        with open(outfile, "a") as f1:
            f1.writelines(lines)


bundle_big_js = ["static/js/bowser.js", "static/js/clockpicker.js", "static/js/mdbootstrap-2.js", "static/js/mdstepper.js"]
outfile_big = "static/js/bundle2.js"

with open(outfile_big, "w") as f:
    f.write("")

for file in bundle_big_js:
    with open(file) as f:
        lines = f.readlines()
        with open(outfile_big, "a") as f1:
            f1.writelines(lines)

bundle_big_css = ["static/css/mdbootstrap.css", "static/css/mdstepper.css"]
outfile_big_css = "static/css/bundle2.css"

with open(outfile_big_css, "w") as f:
    f.write("")

for file in bundle_big_css:
    with open(file) as f:
        lines = f.readlines()
        with open(outfile_big_css, "a") as f1:
            f1.writelines(lines)

bundle_css = ["static/css/center.css", "static/css/clockpickerfix.css", "static/css/docbatch.css"]
outfile_css = "static/css/bundle.css"

with open(outfile_css, "w") as f:
    f.write("")

for file in bundle_css:
    with open(file) as f:
        lines = f.readlines()
        with open(outfile_css, "a") as f1:
            f1.writelines(lines)