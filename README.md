# DOCBatch 3
The DOCBatch that refined DOCBatch. This is DOCBatch 3 - my best project in a while.

# Spaghet-o-meter
All projects made before late 2020 were focused more on development speed rather than reusable code. DOCBatch 3 definitely takes vast steps in being a modular major project. There's some areas with a bit of spaghetti code. Maintaining DOCBatch 3 during its lifetime was relatively easy!

1.25/10 on my Spaghet-o-meter.

# What is DOCBatch 3?
DOCBatch 3 is the 3rd version of the WPI Dine On Campus Batch Reserver Tool, otherwise known as DOCBatch.

DOCBatch 2's readme has more of the story behind DOCBatch's creation, so read that if you need some context. In short - needed dining reservations on campus, app sucked, made web wrapper.

DOCBatch 2 definitely was a step up from the Dine On Campus app, but as I added more features, the UI was starting to get really cluttered, and it was time for a redesign.

Over winter break, I worked on making DOCBatch 3 with a few goals in mine:
* Redesign DOCBatch for a MUCH sleeker experience, keeping all the main operations on a single "screen", while also staying optimized for mobile.
* Refactor the entire backend codebase, while also maintaining feature parity.
* Have something to do over winter break

And DOCBatch 3 happened.

# Why make DOCBatch 3?
To solve a few problems:
* DOCBatch 2's aging UI
* Making it even easier to get food (Eat Now feature)
* Retain the original DOCBatch functionality in a modern design

DOCBatch remained useful into the final days of spring semester, although past v3.1, it was mostly refining features.

# Setup
DOCBatch 3 is an exclusively front-end system that uses Flask templating. This means you'll need to set up a WSGI server to serve DOCBatch. No extra libraries are needed - Flask is just used to do templating.

The WSGI server does not need any additional configuration beyond the basics. A basic WSGI file is included to help you get started. Running in daemon mode is recommended for better performance.

A copy of Material Design Bootstrap 4, including the vertical stepper, time picker and date picker are needed. You will need to obtain this yourself, putting all the necessary JS/CSS into bundle2.js and bundle2.css.

## Bundle scripts
A script is provided called bundle.py. This simply puts all JS and CSS files into one "bundle" file to reduce the number of requests to the server.

bundle2.js/bundle2.css is a lot of the heavy CSS/JS - that's where you'll need to get your copy of MDB 4.

You can customize the code in bundle.py so that you bundle the files that you want. Just modify the array.

You'll also likely want a copy of the DOCBatch API, which is how DOCBatch 3 gets data for certain features.

Once complete, you can then get DOCBatch up and running. 

## Note - Development Headers
DOCBatch 3 has two sets of headers, `headers-bundle.html` and `headers.html`. In the `base.html` file, you can choose which headers to use (bundled or unbundled).

If you're doing development work - use unbundled. Otherwise, use bundled.

## Note - icons
Icons are not included in the repository. You'll need to specify 4 icons in the icons folder, using these resolutions and filenames.

`apple-225x225.png` at 225x225

`apple-337x337.png` at 337x337

`apple-450x450.png` at 450x450

`doc-196.png` at 196x196

# Siri Shortcuts
DOCBatch 3 has some Siri Shortcuts for reserving for now. Those aren't covered under the DOCBatch 3 license, and are free for anyone to copy/reproduce/do whatever with.

# Privacy
DOCBatch 3 has enhanced privacy compared to DOCBatch 2, in the form of direct User ID entry into DOCBatch 3.

# License
DOCBatch 3 is licensed under the LGPL v3.0 license as a major web application.

DOCBatch 3, similar to DOCBatch 2, spent the entirety of its time closed-source. The intention of DOCBatch 3 was to improve upon Dine On Campus' reservation system.

The reason DOCBatch 3 is under the LGPL (compared to DB2's AGPL) is an experiment. I've been wanting to use the LGPL for projects now for a while, so this is the first shot at it.

This also means that you can implement DB3 into a website and not need to open-source the entire thing (just your modifications).

Of course, you'll need to get your own copy of MD Bootstrap 4 with the required vertical stepper, time picker, and date picker components. 

Make sure you get MDBootstrap's version, a portion of DB3 was designed specifically to work with the stupidity of their code.

DOCBatch 3 includes Clippy.js and Bowser.js. Both of these libraries are licensed under the MIT License.

DOCBatch 3 includes Font Awesome, which is GPL compatible and licensed under the Font Awesome Free License.