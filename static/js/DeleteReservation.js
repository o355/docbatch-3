/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

class DeleteReservation {
    constructor (auth, remaininglabel, totallabel, includefuture) {
        this.auth = auth
        this.remaininglabel = remaininglabel
        this.totallabel = totallabel
        this.includefuture = includefuture
        var url = "https://api.dineoncampus.com/v1/customer/reservations?customer_id=" + this.auth.userid
        var xhr = new XHRwrap(url, "GET", true, 30000, null, this.reservlist_callback.bind(this))
        xhr.request()
        eventctrl.emit(eventctrl.deleteReservationsInit)
        document.addEventListener(eventctrl.deleteReservationsP1SpamConfirm.type, this.deletereservation.bind(this, 0))
        consolelog("DeleteReservation", "Beginning deleting reservations.")
    }

    reservlist_callback(response) {
        var data = response.responseText
        try {
            var obj = JSON.parse(data)
            var status = obj['status']
        } catch (error) {
            consolelog("DeleteReservation/reservlist_callback", "Failed Phase 1, JSON parse error")
            eventctrl.emit(eventctrl.deleteReservationsP1Fail)
            return
        }

        if (obj['status'] == "success") {
            this.reservationids = []
            for (var i = 0; i < obj['reservations'].length; i++) {
                if (this.includefuture == false) {
                    var reserv_moment = moment(obj['reservations'][i]['date'])
                    reserv_moment.subtract(3, "hours")
                    reserv_moment.hour(obj['reservations'][i]['hour'])
                    reserv_moment.minute(obj['reservations'][i]['minute'])
                    var current_moment = moment()
                    if (current_moment.diff(reserv_moment) >= 7200000) {
                        this.reservationids.push(obj['reservations'][i]['id'])
                    }
                } else {
                    this.reservationids.push(obj['reservations'][i]['id'])
                }
            }
            consolelog("DeleteReservation/reservlist_callback", "Found " + this.reservationids.length + " candidates for deletion")
            if (this.reservationids.length == 0) {
                eventctrl.emit(eventctrl.deleteReservationsP1None)
                if (this.includefuture == false) {
                    $(".deleteall-pastspan").show()
                }
                return
            } else if (this.reservationids.length >= 20) {
                consolelog("DeleteReservation/reservlist_callback", "User has 20+ reservations, confirming...")
                setTimeout(function () {
                    eventctrl.emit(eventctrl.deleteReservationsP1SpamWarn)
                    $(".s6-deleteall-spam-number").html(this.reservationids.length)
                }.bind(this), 500)
                return
            }
            eventctrl.emit(eventctrl.deleteReservationsP1Success)
            this.deletereservation(0)
        } else {
            consolelog("DeleteReservation/reservlist_callback", "Request status was not success, failing at Phase 1")
            eventctrl.emit(eventctrl.deleteReservationsP1Fail)
            return
        }
    }

    deletereservation(index) {
        if (index == undefined) {
            index = 0
        }
        consolelog("DeleteReservation/deletereservation", "Deleting reservation at index " + index)
        $(this.remaininglabel).html(index)
        $(this.totallabel).html(this.reservationids.length)
        var id = this.reservationids[index]
        var url = "https://api.dineoncampus.com/v1/reservations/?reservation_id=" + id + "&customer_id=" + this.auth.userid
        var xhr = new XHRwrap(url, "DELETE", true, 30000, null, this.deletereservation_callback.bind(this))
        xhr.request()
    }

    deletereservation_callback(request) {
        var data = request.responseText
        try { 
            var obj = JSON.parse(data)
            var status = obj['status']
        } catch (error) {
            eventctrl.emit(eventctrl.deleteReservationsFail)
            consolelog("DeleteReservation/deletereservation_callback", "Failed to confirm delete reservation at JSON parse, failing process.")
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("I'm sorry I couldn't delete your reservations. Make sure your connection to AOL is strong!")
            }
            return
        }

        if (obj['status'] != "success") {
            eventctrl.emit(eventctrl.deleteReservationsFail)
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("I'm sorry I couldn't delete your reservations. Make sure your connection to AOL is strong!")
            }
            consolelog("DeleteReservation/deletereservation_callback", "Delete reservation was not successful, failing process.")
            return
        } else {
            var curindex = $(this.remaininglabel).html()
            var total = $(this.totallabel).html()
            if (parseInt(curindex) + 1 == parseInt(total)) {
                this.remaininglabel.html(parseInt(curindex) + 1)
                consolelog("DeleteReservation/deletereservation_callback", "No more reservations to delete, finishing process.")
                setTimeout(function () {
                    eventctrl.emit(eventctrl.deleteReservationsSuccess)
                    if (clippymode) {
                        clippyagent.stop()
                        clippyagent.play("Congratulate")
                        clippyagent.speak("I'm so glad I could help delete some reservations. Enjoy your email spam!")
                    }
                }.bind(this), 500)

            } else {
                this.deletereservation(parseInt(curindex) + 1)
            }
        }
    }
}