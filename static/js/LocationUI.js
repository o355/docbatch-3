/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

class LocationUI {
    constructor (label, step, content, formname, nextui, feedback) {
        this.label = label
        this.step = step
        this.content = content
        this.formname = formname
        this.nextui = nextui
        this.feedback = feedback
        document.addEventListener(eventctrl.locationSelected.type, this.validate.bind(this))
        consolelog("LocationUI", "New LocationUI object is being constructed.")
    }

    validate() {
        if ($("input[name=" + this.formname + "]:checked").val() != undefined) {
            $(this.step).removeClass("wrong")
            $(this.feedback).hide()
            consolelog("LocationUI/validate", "Good validation.")
            return true
        } else {
            $(this.step).addClass("wrong")
            $(this.feedback).show()
            if (clippymode) {
                if (Math.random() * 10 <= 5) {
                    clippyagent.stop()
                    clippyagent.speak("It seems like you're having trouble selecting a location. Have you thought about Morgan Dining Hall?")
                } else {
                    clippyagent.stop()
                    clippyagent.speak("It seems like you're having trouble selecting a location. Have you considered the Campus Center Food Court?")
                }
            }
            consolelog("LocationUI/validate", "Bad validation.")
            return false
        }
    }

    updatelabel(string, onclick) {
        if (string == undefined) {
            if (onclick) {
                this.validate()
            }
            setTimeout(function() {
                if ($(this.nextui.content).is(":visible")) {
                    var location = $("input[name=" + this.formname + "]:checked").val()
                    consolelog("LocationUI/updatelabel", "Updating label to " + locations.getlocationname(location))
                    $(this.label).attr("data-step-label", locations.getlocationname(location));
                }
            }.bind(this), 10)
        } else {
            consolelog("LocationUI/updatelabel", "Updating label to " + string)
            $(this.label).attr("data-step-label", string)
        }
    }

    disable(state) {
        consolelog("LocationUI/disable", "Disabling with state: " + state)
        if (state) {
            $(this.step).addClass("disabled")
        } else {
            $(this.step).removeClass("disabled")
        }
    }
}