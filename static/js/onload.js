/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

/* eslint-disable no-delete-var */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
var uictrl;
var auth;
var locations;
var frmctrl;
var s1ctrl;
var s2ctrl;
var s3ctrl;
var s4ctrl;
var s5ctrl;
var s6ctrl;
var eatctrl;
var dow = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
var reservations;
var appearance;
var dakamode;
var clippyagent;
var clippyinterval;
var clippymode;

console.log("%cHey! Welcome to DOCBatch!", "font-size: 24px")
console.log("%cIf you're wondering how DOCBatch works behind the scenes, there's lots of internal logging about the operations of DOCBatch.", "font-size: 18px")
console.log("%cEnjoy!", "font-size: 18px")

// https://stackoverflow.com/a/901144
function getParameterByName(name, url = window.location.href) {
  if (!url) url = window.location.href;
  name = name.replace(/[[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var browser = bowser.getParser(window.navigator.userAgent);
var plattype = browser.getPlatformType()
consolelog("onload", "Platform type is " + plattype)
$(function () {
  consolelog("onload", "DOM has loaded!")
    $('.stepper').mdbStepper();
    $("#deleteall-checkbox").prop("checked", false)

    if (plattype != "desktop") {
      consolelog("onload", "Platform type is not desktop, hiding login instructions")
      $("#uid-desktop-instructions").hide()
      $("#uid-notdesktop").show()
    }

    if (localStorage.getItem("appearance") == null) {
      localStorage.setItem("appearance", "automatic")
    }

    if (localStorage.getItem("clippymode") == null) {
      localStorage.setItem("clippymode", "false")
    }

    if (localStorage.getItem("clippymode") == "false") {
      clippymode = false
    } else if (localStorage.getItem("clippymode") == "true") {
      clippymode = true
    }

    if (clippymode) {
      clippy.load("Clippy", function (agent) {
        clippyagent = agent;
      })

      $("#clippymode").html("On")
    }

    initDOCBatch(true)

    $('.datepicker').datepicker({
      format: "mm/dd/yyyy",
      today: "Today",
      clear: "Clear",
      close: "Close"
    });
    
    auth.local_login()

    document.addEventListener(eventctrl.successLoad.type, function () {
      if (clippymode) {
        console.log("Clippy go!")
        setTimeout(function () {
          clippyagent.show()
          if (window.innerWidth >= 600) {
            clippyagent.moveTo(window.innerWidth * 0.85, window.innerHeight * 0.85)
          } else {
            clippyagent.moveTo(window.innerWidth * 0.50, window.innerHeight * 0.85)
          }
          clippyagent.speak("Welcome to DOCBatch! I'm Clippy, and I'm here to help you make dining reservations.")
        }, 1000)

        clippyinterval = setInterval(function() {
          console.log("Clippy animate!")
          clippyagent.animate()
        }, 15000)
      }
    })
})

function initDOCBatch(fromdom) {
  consolelog("initDOCBatch", "DOCBatch is being init'd")
  
  appearance = new Appearance("static/css/dark.css", $("#floating-appearance"), $("#basehtml"))
  appearance.init(true)
  
  initdakamode()

  if (fromdom) {
    $("#loader").show()
  }
  uictrl = new UIController($("#loader"), $("#mainsteps"), $("#login"), $("#step6"), $("#failedload"), $("#step6-success"), $("#step6-fail"), $("#s6-deleteall-p1"), $("#s6-deleteall-p1fail"), $("#s6-deleteall-p1none"), $("#s6-deleteall"), $("#s6-deleteall-fail"), $("#s6-deleteall-success"), $("#login-userid"), $("#eatnow-locating"), $("#eatnow-failed"), $("#eatnow-nothing"), $("#s6-deleteall-spam"), $("#aprilfools"), $("#shutdown"))
  auth = new Authentication("userid", $("#login-email"), $("#login-password"), $("#login-submit"), $("#login-userid-field"), $("#login-submit-userid"))
  locations = new Locations("https://api.docbatch.app/api/v1/locations", $(".fl-ec"), $(".fl-expl"), $("#locations-form"), "radiocopy")
  s2ctrl = new DateUI($("#s2step-label"), $("#step2"), $("#s2content"), $("#s2-datepick"), $("#s2-invalidfeedback"))
  s1ctrl = new LocationUI($("#s1step-label"), $("#step1"), ("#s1content"), "locations-form", s2ctrl, $("#s1-invalidfeedback"))
  s3ctrl = new TimeUI($("#s3step-label"), $("#step3"), $("#s3content"), $("#s3-timepick"), $("#s3-invalidfeedback"))
  s4ctrl = new ReoccurUI($("#s4step-label"), $("#step4"), $("#s4content"), $("#s4-reoccurpick"), "s4-datepicker")
  s5ctrl = new ConfirmUI($("#s5step-label"), $("#step5"), $("#s5content"), $("#s5-reservcount"), $("#s5-reservationtable"), $("#s5-currentreservations-body"))
  s6ctrl = new SubmissionUI($("#s6-remaining"), $("#s6-total"))
  reservations = new Reservations(s1ctrl, s2ctrl, s3ctrl, s4ctrl, s5ctrl, s6ctrl, auth, $("#s1-skiptos5"))
  frmctrl = new FormUIController(s1ctrl, s2ctrl, s3ctrl, s4ctrl, s5ctrl, $("#feedback-step"), reservations,  locations, $("#rfnclosedModal"))
  eatctrl = new EatNow(s1ctrl, frmctrl, locations)
  frmctrl.clearinputs()
  consolelog("initDOCBatch", "DOCBatch has been initalized!")
}

function testingtesting() {
  console.log("Test!")
}

function initdakamode() {
  if (localStorage.getItem("dakamode") == null) {
    localStorage.setItem("dakamode", "false")
  }

  if (localStorage.getItem("dakamode") == "true") {
    dakamode = true
  } else if (localStorage.getItem("dakamode") == "false") {
    dakamode = false
  }

  if (dakamode) {
    $(function () {
      $(".dakamode").show()
      $(".notdakamode").hide()
      $("#dakamode").html("On")
    })
  }
}

function dakatoggle() {
  if (localStorage.getItem("dakamode") == "false") {
    localStorage.setItem("dakamode", "true")
  } else if (localStorage.getItem("dakamode") == "true") {
    localStorage.setItem("dakamode", "false")
  }

  window.location.reload()
}

function clippytoggle() {
  if (localStorage.getItem("clippymode") == "false") {
    localStorage.setItem("clippymode", "true")
  } else if (localStorage.getItem("clippymode") == "true") {
    localStorage.setItem("clippymode", "false")
  }

  window.location.reload()
}

function hm_tostring(hour, minute) {
  var denom;
  if (hour >= 12) {
    denom = "PM"
  } else {
    denom = "AM"
  }
  if (hour >= 13) {
    hour = hour - 12
  } else if (hour == 0) {
    hour = 12
  }

  if (minute <= 9) {
    minute = "0" + minute
  }

  return hour + ":" + minute + " " + denom
}

function deleteAll() {
  consolelog("deleteAll", "Deleting all reservations.")
  deleteall = new DeleteReservation(auth, $("#s6-delall-remaining"), $("#s6-delall-total"), $("#deleteall-checkbox").is(":checked"))
}

function fillUID() {
  consolelog("fillUID", "Filling UID (wmi-span)")
  $("#wmi-span").html(localStorage.getItem("userid"))
}

function db3warndismiss() {
  localStorage.setItem("db3warndismiss", "true")
}

function db3shutdownupdate() {
  var calculated_duration = 1621278000000 - new Date().getTime()
  var moment_duration = moment.duration(calculated_duration)
  $("#db3-shutdown-duration").html(moment_duration.humanize(true, {h: 97}))
}

function clearDelete() {
  $("#deleteall-checkbox").prop("checked", false)
}