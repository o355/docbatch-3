/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

class ConfirmUI {
    constructor (label, step, content, reservcount, collapse, tablebody) {
        this.label = label
        this.step = step
        this.content = content
        this.reservcount = reservcount
        this.collapse = collapse
        this.tablebody = tablebody
        consolelog("ConfirmUI", "A new confirm UI object is being created.")
    }

    updatelabel(string) {
        if (string != undefined) {
            $(this.label).attr("data-step-label", string)
            $(this.reservcount).html(string)
            consolelog("ConfirmUI/updatelabel", "Label updating to " + string)
        }
        consolelog("ConfirmUI/updatelabel", "Label is being updated to " + string)
    }

    updatetable(reservations, collapse) {
        consolelog("ConfirmUI/updatetable", "Table is being updated with " + reservations.reservations.length + " reservations, collapse set to " + collapse)
        $(this.tablebody).empty()
        for (var i = 0; i < reservations.reservations.length; i++) {
            var momentobj = reservations.reservations[i].momentobj
            var location = locations.getlocationname(reservations.reservations[i].location)
            var date = momentobj.format("LL")
            var time = momentobj.format("LT")
            $(this.tablebody).append("<tr><td>" + date + "</td><td>" + time + "</td><td>" + location + "</td><td><a class='btn btn-sm btn-danger waves-effect' onclick='reservations.deletereservation(" + i + ")'><i class='fas fa-trash'></i></a></td>")
        }
        if (collapse) {
            if (reservations.reservations.length <= 5) {
                $(this.collapse).collapse("show")
            } else {
                $(this.collapse).collapse("hide")
            }
        }
    }

    collapsechange(state) {
        consolelog("ConfirmUI/collapsechange", "Collapse state being set to " + state)
        if (state) {
            $(this.collapse).collapse("show")
        } else {
            $(this.collapse).collapse("hide")
        }
    }

    disable(state) {
        consolelog("ConfirmUI/disable", "ConfirmUI step disabling: " + state)
        if (state) {
            $(this.step).addClass("disabled")
        } else {
            $(this.step).removeClass("disabled")
        }
    }
}