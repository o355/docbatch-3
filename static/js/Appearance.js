/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

class Appearance {
    constructor(stylesheet, feedbackspan, bodyhtml) {
        this.stylesheet = stylesheet
        this.feedbackspan = feedbackspan
        this.bodyhtml = bodyhtml
        this.appearance = localStorage.getItem("appearance")
        this.pcs = window.matchMedia('(prefers-color-scheme: dark)')
        window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', this.auto_change.bind(this));
        if (this.appearance == "automatic") {
            $(this.feedbackspan).html("Automatic")
        } else if (this.appearance == "light") {
            $(this.feedbackspan).html("Light")
        } else if (this.appearance == "dark") {
            $(this.feedbackspan).html("Dark")
        }
        consolelog("Appearance", "A new apperance object is being initialized")
        document.addEventListener(eventctrl.appearanceChanged.type, this.manual_change.bind(this))
    }

    init(onload) {
        consolelog("Appearance/init", "Called with onload: " + onload)
        if (this.appearance == "automatic") {
            this.auto_change()
        } else if (this.appearance == "light") {
            consolelog("Appearance/init", "Light mode is now active.")
            $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', true);
        } else if (this.appearance == "dark") {
            consolelog("Appearance/init", "Dark mode is now active.")
            $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', false);
        }

        if (onload) {
            consolelog("Appearance/init", "removing default bg color (from onload)")
            $(this.bodyhtml).css("background-color", "")
        }
    }

    auto_change() {
        if (this.appearance == "automatic") {
            if ((window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
                consolelog("Appearance/auto_change", "Going into dark mode from auto change")
                $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', false);
            } else {
                consolelog("Appearance/auto_change", "Going into light mode from auto change")
                $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', true);
            }
        }
    }

    manual_change() {
        if (this.appearance == "light") {
            consolelog("Appearance/manual_change", "Going from Light -> Dark")
            this.appearance = "dark"
            localStorage.setItem("appearance", "dark")
            $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', false);
            $(this.feedbackspan).html("Dark")
        } else if (this.appearance == "dark") {
            consolelog("Appearance/manual_change", "Going from Dark -> Automatic")
            this.appearance = "automatic"
            localStorage.setItem("appearance", "automatic")
            $(this.feedbackspan).html("Automatic")
            this.auto_change()
        } else if (this.appearance == "automatic") {
            consolelog("Appearance/manual_change", "Going from Automatic -> Light")
            this.appearance = "light"
            localStorage.setItem("appearance", "light")
            $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', true);
            $(this.feedbackspan).html("Light")
        }
    }
}