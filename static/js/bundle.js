function consolelog(caller, message) {
    console.log("%c[" + caller + "]%c " + message, "font-weight: bold; color: purple", "")
}/**
 * Orchestrates the DOM Events triggered throughout DOCBatch. Mainly to
 * prevent hardcoded event names, and rather references to a single object.
 */
// eslint-disable-next-line no-unused-vars
class Events {
    constructor() {
        this.initLoginSuccess = new Event("logininit-good")
        this.initLoginFailure = new Event("logininit-bad")
        this.loginSuccess = new Event("login-good")
        this.loginFailure = new Event("login-bad")
        this.loginAttempt = new Event("login-attempt")
        this.failedLoad = new Event("failed-load")
        this.successLoad = new Event("success-load")
        this.makeReservations = new Event("make-reservations")
        this.makeReservationsFail = new Event("make-reservations-fail")
        this.makeReservationsSuccess = new Event("make-reservations-success")
        this.deleteReservationsInit = new Event("delete-reservations-init")
        this.deleteReservationsP1Fail = new Event("delete-reservations-p1fail")
        this.deleteReservationsP1Fail2 = new Event("delete-reservations-p1fail2")
        this.deleteReservationsP1None = new Event("delete-reservations-p1none")
        this.deleteReservationsP1None2 = new Event("delete-reservations-p1none2")
        this.deleteReservationsP1Success = new Event("delete-reservations-p1success")
        this.deleteReservationsFail = new Event("delete-reservations-fail")
        this.deleteReservationsFail2 = new Event("delete-reservations-fail2")
        this.deleteReservationsSuccess = new Event("delete-reservations-success")
        this.deleteReservationsSuccess2 = new Event("delete-reservations-success2")
        this.deletePendingReservations = new Event("delete-pending-reservations")
        this.logOut = new Event("logout")
        this.backToStart = new Event("backtostart")
        this.reinitSuccess = new Event("reinit-success")
        this.reinitFailure = new Event("reinit-failure")
        this.loginSwitchUserID = new Event("login-switchuserid")
        this.loginSwitchNormal = new Event("login-switchnormal")
        this.loginUserIDAttempt = new Event("login-useridattempt")
        this.loginUserIDSuccess = new Event("login-useridsuccess")
        this.eatNowStart = new Event("eatnow-start")
        this.eatNowFailed = new Event("eatnow-failed")
        this.eatNowNothing  = new Event("eatnow-nothing")
        this.eatNowSuccess = new Event("eatnow-success")
        this.eatNowFailedReset = new Event("eatnow-failed-reset")
        this.eatnowNothingReset = new Event("eatnow-nothing-reset")
        this.locationSelected = new Event("location-selected")
        this.appearanceChanged = new Event("appearance-changed")
        this.deleteReservationsP1SpamWarn = new Event("delete-reservations-p1-spamwarn")
        this.deleteReservationsP1SpamDeny = new Event("delete-reservations-p1-spamdeny")
        this.deleteReservationsP1SpamConfirm = new Event("delete-reservations-p1-spamconfirm")
        this.aprilFoolsFadeIn = new Event("april-fools-fadein")
        this.aprilFoolsFadeOut = new Event("april-fools-fadeout")
        this.shutdownFade = new Event("shutdown-fadein")
        consolelog("Events", "New Events object has been created.")
    }

    emit(event) {
        consolelog("Events/emit", "Emitting event " + event.type)
        document.dispatchEvent(event)
    }
}

// eslint-disable-next-line no-unused-vars
var eventctrl = new Events()// eslint-disable-next-line no-unused-vars
class BasicUIController {
    constructor(loadingelem, stepelem) {
        this.loadingelem = loadingelem
        this.stepelem = stepelem
    }

    loadDone() {
        $(this.loadingelem).fadeOut(500);
        $(this.stepelem).fadeIn(500)
    }
}

/**
 * Controls the UI of DOCBatch, including transitions between steps, etc.
 */
// eslint-disable-next-line no-unused-vars
class UIController {
    /**
     * Constructs a new UIController.
     * @param {*} le The loading element to control.
     * @param {*} se The steps element to control.
     * @param {*} lge The login element to control.
     * @param {*} sue The submission element to control.
     * @param {*} fle The failed loading element to control.
     * @param {} ec A variable that can be written to by other classes for when errors occur as the error code.
     * @param {} msg A variable that can be written to by other classes for when errors occur as the message for the error.
     */
    constructor(le, se, lge, sue, fle, suesucc, suefail, dap1, dap1fail, dap1none, da, dafail, dasuccess, ulge, eatl, eatf, eatn, dap1spam, af, shutdown) {
        this.le = le;
        this.se = se;
        this.lge = lge;
        this.sue = sue;
        this.doc = document;
        this.fle = fle;
        this.suesucc = suesucc
        this.suefail = suefail
        this.dap1 = dap1
        this.dap1fail = dap1fail
        this.dap1none = dap1none
        this.da = da
        this.dafail = dafail
        this.dasuccess = dasuccess
        this.ulge = ulge
        this.eatl = eatl
        this.eatf = eatf
        this.eatn = eatn
        this.dap1spam = dap1spam
        this.af = af
        this.shutdown = shutdown
        // beautiful, ain't it?
        document.addEventListener(eventctrl.reinitSuccess.type, this.fadecontrol.bind(this, 500, this.le, this.suesucc))
        document.addEventListener(eventctrl.reinitFailure.type, this.fadecontrol.bind(this, 500, this.le, this.suefail))
        document.addEventListener(eventctrl.failedLoad.type, this.fadecontrol.bind(this, 500, this.fle, this.le))
        document.addEventListener(eventctrl.successLoad.type, this.fadecontrol.bind(this, 500, this.se, this.le))
        document.addEventListener(eventctrl.loginSuccess.type, this.fadecontrol.bind(this, 500, this.le, this.lge))
        document.addEventListener(eventctrl.initLoginFailure.type, this.fadecontrol.bind(this, 500, this.lge, this.le))
        document.addEventListener(eventctrl.makeReservations.type, this.fadecontrol.bind(this, 500, this.sue, this.se))
        document.addEventListener(eventctrl.makeReservationsFail.type, this.fadecontrol.bind(this, 500, this.suefail, this.sue))
        document.addEventListener(eventctrl.deleteReservationsInit.type, this.fadecontrol.bind(this, 500, this.dap1, this.se))
        document.addEventListener(eventctrl.deleteReservationsFail.type, this.fadecontrol.bind(this, 500, this.dafail, this.da))
        document.addEventListener(eventctrl.deleteReservationsFail2.type, this.fadecontrol.bind(this, 500, this.se, this.dafail))
        document.addEventListener(eventctrl.makeReservationsSuccess.type, this.fadecontrol.bind(this, 500, this.suesucc, this.sue))
        document.addEventListener(eventctrl.deleteReservationsP1None.type, this.fadecontrol.bind(this, 500, this.dap1none, this.dap1))
        document.addEventListener(eventctrl.deleteReservationsP1Fail.type, this.fadecontrol.bind(this, 500, this.dap1fail, this.dap1))
        document.addEventListener(eventctrl.deleteReservationsP1None2.type, this.fadecontrol.bind(this, 500, this.se, this.dap1none))
        document.addEventListener(eventctrl.deleteReservationsP1Fail2.type, this.fadecontrol.bind(this, 500, this.se, this.dap1fail))
        document.addEventListener(eventctrl.deleteReservationsSuccess.type, this.fadecontrol.bind(this, 500, this.dasuccess, this.da))
        document.addEventListener(eventctrl.deleteReservationsSuccess2.type, this.fadecontrol.bind(this, 500, this.se, this.dasuccess))
        document.addEventListener(eventctrl.deleteReservationsP1Success.type, this.fadecontrol.bind(this, 500, this.da, this.dap1))
        document.addEventListener(eventctrl.loginSwitchUserID.type, this.fadecontrolsync.bind(this, 500, this.ulge, this.lge))
        document.addEventListener(eventctrl.loginSwitchNormal.type, this.fadecontrolsync.bind(this, 500, this.lge, this.ulge))
        document.addEventListener(eventctrl.loginUserIDSuccess.type, this.fadecontrol.bind(this, 500, this.le, this.ulge))
        document.addEventListener(eventctrl.eatNowStart.type, this.fadecontrol.bind(this, 100, this.eatl, this.se))
        document.addEventListener(eventctrl.eatNowFailed.type, this.fadecontrol.bind(this, 500, this.eatf, this.eatl))
        document.addEventListener(eventctrl.eatNowNothing.type, this.fadecontrol.bind(this, 500, this.eatn, this.eatl))
        document.addEventListener(eventctrl.eatNowSuccess.type, this.fadecontrol.bind(this, 500, this.se, this.eatl))
        document.addEventListener(eventctrl.eatnowNothingReset.type, this.fadecontrol.bind(this, 500, this.se, this.eatn))
        document.addEventListener(eventctrl.eatNowFailedReset.type, this.fadecontrol.bind(this, 500, this.se, this.eatf))
        document.addEventListener(eventctrl.deleteReservationsP1SpamWarn.type, this.fadecontrol.bind(this, 500, this.dap1spam, this.dap1))
        document.addEventListener(eventctrl.deleteReservationsP1SpamDeny.type, this.fadecontrol.bind(this, 500, this.se, this.dap1spam))
        document.addEventListener(eventctrl.deleteReservationsP1SpamConfirm.type, this.fadecontrol.bind(this, 500, this.da, this.dap1spam))
        document.addEventListener(eventctrl.aprilFoolsFadeIn.type, this.fadecontrolsync.bind(this, 500, this.af, this.le))
        document.addEventListener(eventctrl.aprilFoolsFadeOut.type, this.fadecontrolsync.bind(this, 500, this.le, this.af))
        document.addEventListener(eventctrl.shutdownFade.type, this.fadecontrolsync.bind(this, 500, this.shutdown, this.le))

        consolelog("UIController", "UIController constructed.")
    }

    /**
     * Fades between two elements.
     * @param {*} duration The duration of the fade.
     * @param {*} ie The element to fade in.
     * @param {*} oe The element to fade out.
     */
    fadecontrol(duration, ie, oe) {
        consolelog("UIController/fadecontrol", "Fading async in " + $(ie).attr("id") + ", fading out " + $(oe).attr("id") + ", total duration of " + duration + "ms.")
        $(ie).fadeIn(duration)
        $(oe).fadeOut(duration)
    }

    fadecontrolsync(duration, ie, oe) {
        consolelog("UIController/fadecontrolsync", "Fading sync in " + $(ie).attr("id") + ", fading out " + $(oe).attr("id") + ", total duration of " + duration + "ms.")
        $(oe).fadeOut(duration / 2, function() {
            $(ie).fadeIn(duration / 2)
        })
    }

    /**
     * Controls if DOCBatch needs to switch to a failed loader, in the event of a problem.
     * @param {*} errorcode The error code of what happened.
     * @param {*} message An english explanation of the error.
     */
    failedload(errorcode, message) {
        consolelog("UIController/failedload", "Failed load fade.")
        this.fadecontrol(500, this.fle, this.le)
    }

    
}/**
 * Handles authentication with the Dine On Campus servers on first run, along with
 * checking for the User ID.
 */
// eslint-disable-next-line no-unused-vars
class Authentication {
    /**
     * Constructs a new Authentication class.
     * @param {} key The key in the local storage to retrieve the User ID from.
     * @param {*} ee The email element to grab values from when logging in.
     * @param {*} pe The password element to grab values from when logging in.
     * @param {*} se The submit button element.
     */
    constructor(key, ee, pe, se, ue, seue) {
        this.key = key
        this.ee = ee
        this.pe = pe
        this.se = se
        this.ue = ue
        this.seue = seue
        this.se_origcontent = $(this.se).html()
        this.seue_origcontent = $(this.seue).html()
        this.userid = ""
        this.loadingcontent = '<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>'
        $(se).attr("onclick", "eventctrl.emit(eventctrl.loginAttempt)")
        $(seue).attr("onclick", "eventctrl.emit(eventctrl.loginUserIDAttempt)")
        document.addEventListener(eventctrl.loginAttempt.type, this.login.bind(this))
        document.addEventListener(eventctrl.logOut.type, this.logout.bind(this))
        document.addEventListener(eventctrl.loginUserIDAttempt.type, this.login_userid.bind(this))
        consolelog("Authentication", "A new Authentication object has been constructed.")
    }

    /**
     * Does a local login, i.e., logins in via known local storage key.
     */
    local_login() {
        consolelog("Authentication/local_login", "Local Login is being attempted")
        if (localStorage.getItem(this.key) != null) {
            this.userid = localStorage.getItem(this.key);
            consolelog("Authentication/local_login", "Local login success, UID " + this.userid)
            document.dispatchEvent(eventctrl.initLoginSuccess)
            return
        } else {
            consolelog("Authentication/local_login", "Local login failure.")
            document.dispatchEvent(eventctrl.initLoginFailure)
            return
        }
    }

    login_userid() {
        consolelog("Authentication/login_userid", "UID login attempting")
        var fieldval = $(this.ue).val()
        var input_userid = ""
        if (fieldval.includes("%22")) {
            consolelog("Authentication/login_userid", "Parsing UID via Cookie")
            // Parsing the raw cookie value
            var fieldarray = fieldval.split("%22")
            // With current cookie formatting it will be the 3rd element
            input_userid = fieldarray[3]
        } else {
            // Replace potential quotes
            input_userid = fieldval
            input_userid.replace("'", "")
            input_userid.replace('"', "")
        }

        if (input_userid.length != 24) {
            consolelog("Authentication/login_userid", "UID length invalid at length " + input_userid.length)
            this.login_validation_userid(false)
            return
        }

        var url = "https://api.dineoncampus.com/v1/customer/info?customer_id=" + input_userid
        var xhr = new XHRwrap(url, "GET", true, 30000, null, this.login_userid_back.bind(this))
        xhr.request()
        this.login_validation_userid(null)
        $(this.seue).prop("disabled", true)
        $(this.seue).html(this.loadingcontent + "" + this.seue_origcontent)
    }

    login_userid_back(response) {
        try {
            var data = response.responseText;
            var obj = JSON.parse(data);
        } catch {
            consolelog("Authentication/login_userid_back", "Verification failed, failed at parsing JSON")
            this.login_validation_userid(false)
        }

        $(this.seue).prop("disabled", false)
        $(this.seue).html(this.seue_origcontent)

        try {
            var status = obj['status']
            if (status == "success") {
                consolelog("Authentication/login_userid_back", "Verification successful!")
                localStorage.setItem("userid", obj['customer']['id'])
                this.userid = obj['customer']['id']
                this.login_validation_userid(true)
                setTimeout(function () {
                    document.dispatchEvent(eventctrl.loginUserIDSuccess)
                }, 500)
                setTimeout(function () {
                    document.dispatchEvent(eventctrl.initLoginSuccess)
                }, 1000)
                setTimeout(function () {
                    $(this.ue).val("")
                    $(this.ee).val("")
                    $(this.pe).val("")
                }.bind(this), 2000)
            } else {
                consolelog("Authentication/login_userid_back", "Verification failure, didn't see success as status")
                this.login_validation_userid(false)
            }
        } catch {
            consolelog("Authentication/login_userid_back", "Verification failed, failed post-JSON parse")
            this.login_validation_userid(false)
        }
    }

    login_validation_userid(useridvalid) {
        consolelog("Authentication/login_validation_userid", "Updating UID validation, UID valid is " + useridvalid)
        $(this.ue).removeClass("is-invalid").removeClass("is-valid")
        if (useridvalid == true) {
            $(this.ue).addClass("is-valid")
            setTimeout(function() {
                $(this.ue).removeClass("is-valid")
            }.bind(this), 2000)
        } else if (useridvalid == false) {
            $(this.ue).addClass("is-invalid")
        }
    }

    /**
     * Updates validation for login.
     * @param {*} emailvalid 
     * @param {*} passvalid 
     * @param {*} reset 
     */
    login_validation(emailvalid, passvalid) {
        consolelog("Authentication/login_validation", "Updating login validation, emailvalid is " + emailvalid + ", passvalid is " + passvalid)
        $(this.ee).removeClass("is-invalid").removeClass("is-valid")
        $(this.pe).removeClass("is-invalid").removeClass("is-valid")
        // Explicit checking because emailvalid/passvalid can be null (just reset)
        if (emailvalid == true) {
            $(this.ee).addClass("is-valid")
            setTimeout(function() {
                $(this.ee).removeClass("is-valid")
            }.bind(this), 2000)
        } else if (emailvalid == false) {
            $(this.ee).addClass("is-invalid")
        }

        if (passvalid == true) {
            $(this.pe).addClass("is-valid")
            setTimeout(function () {
                $(this.pe).removeClass("is-valid")
            }.bind(this), 2000)
        } else if (passvalid == false) {
            $(this.pe).addClass("is-invalid")
        }
    }

    /**
     * Does a Dine On Campus server login.
     */
    login() {
        consolelog("Authentication/login", "Beginning login process")
        // eslint-disable-next-line no-undef
        var token = uuidv4();
        var url = "https://api.dineoncampus.com/v1/customer/authenticate.json?email=" + $(this.ee).val() + "&password=" + $(this.pe).val() + "&token=" + token 
        var xhr = new XHRwrap(url, "GET", true, 30000, null, this.loginback.bind(this))
        xhr.request()
        this.login_validation(null, null)
        $(this.se).prop("disabled", true)
        $(this.se).html(this.loadingcontent + "" + this.se_origcontent)
    }

    logout() {
        consolelog("Authentication/logout", "Logging out!")
        localStorage.removeItem("userid")
        localStorage.removeItem("appearance")
        localStorage.removeItem("dakamode")
        localStorage.removeItem("clippymode")
        location.reload()
    }

    /**
     * Callback function for XHRwrap.
     * @param {} request request object from XHRResposeObject
     */
    loginback(request) {
        try {
            var data = request.responseText;
            var obj = JSON.parse(data)
        } catch {
            this.login_validation(false, false)
            consolelog("Authentication/loginback", "Failed to login in, failed during JSON parse")
        }

        $(this.se).prop("disabled", false)
        $(this.se).html(this.se_origcontent)

        try {
            var userid = obj['customer']['id']
            localStorage.setItem("userid", userid)
            consolelog("Authentication/loginback", "Login successful!")
            this.login_validation(true, true)
            setTimeout(function() {
                document.dispatchEvent(eventctrl.loginSuccess)
            }, 500)
            setTimeout(function() {
                document.dispatchEvent(eventctrl.initLoginSuccess)
            }, 1000)
            setTimeout(function() {
                $(this.ee).val("")
                $(this.pe).val("")
                $(this.ue).val("")
            }.bind(this), 2000)
        } catch {
            this.login_validation(false, false)
            consolelog("Authentication/loginback", "Login failed post-JSON parse")
        }
    }
}/**
 * Handles the Dine On Campus locations. Provides translation between names & IDs, and vice versa.
 */
class Locations {
    /**
     * Constructs a new Locations object.
     * @param {} url The URL of the DOCBatch API to call to get location data.
     * @param {} ec An element (or class name) that can be written to for error code purposes.
     * @param {} msg An element (or class name) that can be written to for error code purposes.
     * @param {} locationsform An element to serve as the locations form, to which locations will be written to.
     * @param {} radiocopy The ID (NOT JQUERY REFERENCE) of the element to copy to in the locations form. Must also be the prefix for subelements (_hours, _location)
     */
    constructor (url, ec, msg, locationsform, radiocopy) {
        this.url = url
        this.ec = ec
        this.msg = msg
        this.locations = {}
        this.locationsarray = []
        this.locationsform = locationsform
        this.radiocopy = radiocopy
        this.opencolor = "greentext"
        this.partialopencolor = "orangetext"
        this.closedcolor = "redtext"
        document.addEventListener(eventctrl.initLoginSuccess.type, this.init.bind(this))
    }

    /**
     * Initializes the locations database from the URL to be called. This is usually done after a confirmed successful login, but
     * could be done before.
     */
    init() {
        var url = this.url
        var xhr = new XHRwrap(url, "GET", true, 30000, null, this.initcallback.bind(this))
        xhr.request()
    }

    /**
     * Gets a location name from an ID
     * @param {*} id An ID
     */
    getlocationname(id) {
        return this.locationsarray[id]
    }

    getlocations() {
        return this.locations
    }

    getlocationsarray() {
        return this.locationsarray
    }

    reservationsrequired(k) {
        return this.locations[k]['reservations']
    }

    getoperationstatus(k) {
        var timestamp = Math.floor(Date.now() / 1000)
        for (var i = 0; i < this.locations[k]['hours'].length; i = i + 2) {
            var starttime = this.locations[k]['hours'][i]
            var endtime = this.locations[k]['hours'][i + 1]

            if (timestamp >= starttime && timestamp >= endtime) {
                continue
            } else if (timestamp >= starttime && timestamp <= endtime) {
                var seconds_open = endtime - timestamp
                return {"status": "open", "secondsleft": seconds_open}
            } else if (timestamp <= starttime && timestamp <= endtime) {
                var seconds_left = starttime - timestamp
                return {"status": "closed", "secondsleft": seconds_left};
            }
        }
        return {"status": "closed", "secondsleft": -1}
    }

    /**
     * Data from the callback to init Locations.
     * @param {} request XHRHttpRequest request thing.
     */
    initcallback(request) {
        try {
            var data = request.responseText;
            var obj = JSON.parse(data)
        } catch {
            $(this.ec).html("320")
            $(this.msg).html("Failed to load location data.")
            eventctrl.emit(eventctrl.failedLoad)
            return
        }

        var outerHTML = $("#" + this.radiocopy).prop("outerHTML");

        $("#" + this.radiocopy).attr("id", "preventduplicity");
        $("#" + this.radiocopy + "_label").attr("id", "preventduplicity_label")
        $("#" + this.radiocopy + "_input").attr("id", "preventduplicity_input")
        $("#" + this.radiocopy + "_location").attr("id", "preventduplicity_location")
        $("#" + this.radiocopy + "_hours").attr("id", "preventduplicity_hours")

        $(this.locationsform).empty()
        Object.keys(obj['data']).forEach(function (k) {
            if (obj['data'][k]['name'].search("Morgan") != -1 && (localStorage.getItem("dakamode") == "true")) {
                this.locationsarray[k] = "DAKA"
            } else {
                this.locationsarray[k] = obj['data'][k]['name']
            }
            $(this.locationsform).prepend(outerHTML)
            $("#" + this.radiocopy).attr("id", k)
            $("#" + this.radiocopy + "_label").attr("id", k + "_label")
            $("#" + this.radiocopy + "_input").attr("id", k + "_input")
            $("#" + this.radiocopy + "_location").attr("id", k + "_name")
            $("#" + this.radiocopy + "_hours").attr("id", k + "_hours")
            $("#" + k + "_name").html(this.locationsarray[k])
            $("#" + k + "_label").attr("for", k + "_input")
            $("#" + k + "_input").attr("name", "locations-form")
            $("#" + k + "_input").attr("value", k)
            this.locations[k] = {}
            this.locations[k]['hrspan'] = $("#" + k + "_hours")
            this.locations[k]['hours'] = obj['data'][k]['thisweekhours']['L'].concat(obj['data'][k]['nextweekhours']['L']) 
            this.locations[k]['lat'] = obj['data'][k]['lat']
            this.locations[k]['lng'] = obj['data'][k]['lng']
            this.locations[k]['reservations'] = obj['data'][k]['reservations']
            if (obj['data'][k]['reservations'] == false) {
                $("#" + k + "_input").attr("disabled", true)
            }
        }.bind(this))

        $("#preventduplicity").attr("id", this.radiocopy)
        $("#preventduplicity_input").attr("id", this.radiocopy + "_input")
        $("#preventduplicity_location").attr("id", this.radiocopy + "_location")
        $("#preventduplicity_hours").attr("id", this.radiocopy + "_hours")
        window.onfocus = this.updateTimes.bind(this)
        setInterval(this.updateTimes.bind(this), 10000)
        this.updateTimes()
        setTimeout(function() {
            eventctrl.emit(eventctrl.successLoad)
        }, 250)
    }

    /**
     * Update the times.
     */
    updateTimes() {
        var timestamp = Math.floor(Date.now() / 1000)
        var curobj = new Date()
        Object.keys(this.locations).forEach(function (k) {
            for (var i = 0; i < this.locations[k]['hours'].length; i = i + 2) {
                var starttime = this.locations[k]['hours'][i]
                var endtime = this.locations[k]['hours'][i + 1]
                var starttime_dateobj = new Date(starttime * 1000)
                var endtime_dateobj = new Date(endtime * 1000)

                if (timestamp >= starttime && timestamp >= endtime) {
                    continue
                } else if (timestamp >= starttime && timestamp <= endtime) {
                    var seconds_open = endtime - timestamp
                    // eslint-disable-next-line no-undef
                    var closestr = hm_tostring(endtime_dateobj.getHours(), endtime_dateobj.getMinutes())
                    if (seconds_open > 3600) {
                        $(this.locations[k]['hrspan']).html("Open! Closes at " + closestr + ".").removeClass().addClass(this.opencolor)
                    } else {
                        if (seconds_open <= 60) {
                            $(this.locations[k]['hrspan']).html("Open, but closes in " + Math.ceil(seconds_open / 60) + " minute.").removeClass()
                        } else {
                            $(this.locations[k]['hrspan']).html("Open, but closes in " + Math.ceil(seconds_open / 60) + " minutes.").removeClass()
                        }
                        if (seconds_open > 300) {
                            $(this.locations[k]['hrspan']).addClass(this.partialopencolor)
                        } else {
                            $(this.locations[k]['hrspan']).addClass(this.closedcolor)
                        }
                    }
                    return
                } else if (timestamp <= starttime && timestamp <= endtime) {
                    // eslint-disable-next-line no-undef
                    var openstr = hm_tostring(starttime_dateobj.getHours(), starttime_dateobj.getMinutes())
                    // eslint-disable-next-line no-undef
                    var openday = dow[starttime_dateobj.getDay()]
                    var seconds_closed = starttime - timestamp
                    if (starttime - timestamp < 3600) {
                        if (seconds_closed <= 60) {
                            $(this.locations[k]['hrspan']).html("Closed, but opens in " + Math.ceil(seconds_closed / 60) + " minute.").removeClass().addClass(this.closedcolor)
                        } else {
                            $(this.locations[k]['hrspan']).html("Closed, but opens in " + Math.ceil(seconds_closed / 60) + " minutes.").removeClass().addClass(this.closedcolor)
                        }
                    } else if (starttime_dateobj.getDay() == curobj.getDay()) {
                        $(this.locations[k]['hrspan']).html("Closed. Opens at " + openstr + ".").removeClass().addClass(this.closedcolor)
                    } else if (starttime - timestamp < 86400) {
                        $(this.locations[k]['hrspan']).html("Closed. Opens tomorrow at " + openstr + ".").removeClass().addClass(this.closedcolor)
                    } else if (starttime - timestamp < 604800) {
                        $(this.locations[k]['hrspan']).html("Closed. Opens " + openday + " at " + openstr + ".").removeClass().addClass(this.closedcolor)
                    } else {
                        $(this.locations[k]['hrspan']).html("Closed. Opens next " + openday + " at " + openstr + ".").removeClass().addClass(this.closedcolor)
                    }
                    return;
                }
            }
            $(this.locations[k]['hrspan']).html("Closed.").removeClass().addClass(this.closedcolor)
        }.bind(this))
    }

}/**
 * Mostly controls the UI for the labels
**/

class FormUIController {
    constructor(s1ctrl, s2ctrl, s3ctrl, s4ctrl, s5ctrl, stepper, reservations, locations, closedmodal) {
        this.s1ctrl = s1ctrl
        this.s2ctrl = s2ctrl
        this.s3ctrl = s3ctrl
        this.s4ctrl = s4ctrl
        this.s5ctrl = s5ctrl
        this.stepper = stepper
        this.reservations = reservations
        this.locations = locations
        this.closedmodal = closedmodal
        document.addEventListener(eventctrl.backToStart.type, this.backtostart.bind(this))
        consolelog("FormUIController", "A new FormUIController has been created.")
    }

    skips1tos5() {
        consolelog("FormUIController/skips1tos5", "Skipping from Step 1 to 5.")
        $("input[name=" + this.s1ctrl.formname + "]").prop("checked", true)
        $(this.stepper).nextStep()
        $(this.s2ctrl.input).val("1")
        $(this.stepper).nextStep()
        $(this.s3ctrl.input).val("1")
        $(this.stepper).nextStep()
        $(this.s4ctrl.timesinput).val("1")
        $("input[name=" + this.s4ctrl.downame).prop("checked", true)
        $(this.stepper).nextStep()
        $(this.s1ctrl.step).removeClass("done")
        $(this.s2ctrl.step).removeClass("done")
        $(this.s3ctrl.step).removeClass("done")
        $(this.s4ctrl.step).removeClass("done")
        this.disableall_excl5()
    }

    s1validate() {
        var result = this.s1ctrl.validate()
        if (result) {
            consolelog("FormUIController/s1validate", "S1 validated! Moving to next step in 10ms...")
            setTimeout(function() {
                $(this.stepper).nextStep()
                this.s1ctrl.updatelabel()
            }.bind(this), 10)
        } else {
            consolelog("FormUIController/s1validate", "S1 NOT validated. Not moving on to next step.")
            $(this.stepper).nextStep()
        }
        return false
    }

    reservefornow(bypassconfirm) {
        if ($("input[name=" + this.s1ctrl.formname + "]:checked").val() == undefined) {
            consolelog("FormUIController/reservefornow", "Failed to reserve for now, no location selected.")
            this.s1ctrl.validate()
            $(this.stepper).nextStep()
            return
        }
        var location = $("input[name=" + this.s1ctrl.formname + "]:checked").val()
        var statusdict = this.locations.getoperationstatus(location)
        if (statusdict['status'] == "closed" && bypassconfirm != true && statusdict['secondsleft'] > 900) {
            consolelog("FormUIController/reservefornow", "Location " + location + " is closed, bypass false, throwing modal.")
            $(this.closedmodal).modal("show")
            return
        }
        var timestamp = new Date().getTime() / 1000
        var adjusted = Math.round(timestamp / 1800)
        var finalts = adjusted * 1800
        if (statusdict['secondsleft'] <= 900 && statusdict['secondsleft'] != -1 && statusdict['status'] != "closed") {
            finalts = finalts - 1800
        }
        var momentobj = moment.unix(finalts)
        $(this.s2ctrl.input).val(momentobj.format("MM/DD/YYYY"))
        $(this.s3ctrl.input).val(momentobj.format("h:mm A"))
        $(this.s4ctrl.timesinput).val("1")
        consolelog("FormUIController/reservefornow", "Making reservation at " + location + " for " + momentobj.format("MM/DD/YYYY") + " " + momentobj.format("h:mm A"))
        $("input[name=" + this.s4ctrl.downame + "]").prop("checked", true)
        this.undisable_5()
        setTimeout(function() {
            $(this.stepper).nextStep()
            this.s1ctrl.updatelabel()
            $(this.stepper).nextStep()
            this.s2ctrl.updatelabel()
            $(this.stepper).nextStep()
            this.s3ctrl.updatelabel()
            $(this.stepper).nextStep()
            this.s4ctrl.updatelabel()
            this.reservations.createsinglereservation()
            setTimeout(function () {
                this.disableall_excl5()
            }.bind(this), 10)
            setTimeout(function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $(this.s5ctrl.step).offset().top
                }, 10)
            }.bind(this), 300)
        }.bind(this), 10)
    }

    // Validates Step 2, allowing it to move onto the next step. Could probably use jQuery Validator, but meh.
    s2validate(onclick) {
        var result = this.s2ctrl.validate(onclick)
        if (result) {
            consolelog("FormUIController/s2validate", "Validated Step 2! Moving to next step in 10ms.")
            setTimeout(function() {
                $(this.stepper).nextStep()
            }.bind(this), 10)
        }
        return true
    }

    // Updates the text in the Step 3 clock so it's not ugly.
    // Because the built-in callbacks don't work, this gets called every time the field is changed.
    

    // Mostly copy/paste code from above. Using the same technique to allow for proper validation.
    // Difference is in that there's an add 1 reservation or add reocurring, so we have to account for clicking on that button
    // and moving to S5, or just moving to the next step (S4)
    s3validate(onclick, skipsteps) {
        var result = this.s3ctrl.validate(onclick)
        if (result) {
            if (skipsteps) {
                consolelog("FormUIController/s3validate", "S3 is being validated with skipping steps.")
                setTimeout(function () {
                    $(this.s4ctrl.timesinput).val("1")
                    $("input[name=" + this.s4ctrl.downame + "]").prop("checked", true)
                    this.s4ctrl.updatelabel()
                    this.undisable_5()
                    setTimeout(function () {
                        $(this.stepper).nextStep()
                        $(this.stepper).nextStep()
                        this.reservations.createsinglereservation()
                        this.disableall_excl5()
                        setTimeout(function () {
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $(this.s5ctrl.step).offset().top
                            }, 10)
                        }.bind(this), 300)
                    }.bind(this), 10)
                }.bind(this), 10)
            } else {
                consolelog("FormUIController/s3validate", "S3 is being validated without skipping steps.")
                if (onclick != true) {
                    setTimeout(function () {
                        $(this.stepper).nextStep()
                    }.bind(this), 10)
                }
            }
        }
        return false
    }

    s4addbutton() {
        if (parseInt($(this.s4ctrl.timesinput).val()) < parseInt($(this.s4ctrl.timesinput).attr("min")) || parseInt($(this.s4ctrl.timesinput).val()) > parseInt($(this.s4ctrl.timesinput).attr("max"))) {
            consolelog("FormUIController/s4addbutton", "Reservation times input out of range to continue.")
            return
        }

        if ($('input[name="' + this.s4ctrl.downame + '"]:checked').length < 1) {
            consolelog("FormUIController/s4addbutton", "Reservation days checked are none. Not continuing.")
            $(this.stepper).nextStep()
            return
        }

        consolelog("FormUIController/s4addbutton", "S4 validated. Moving on to S5.")
        this.s4ctrl.updatelabel()
        this.undisable_5()
        setTimeout(function () {
            $(this.stepper).nextStep()
            this.reservations.createrecursivereservation()
            this.disableall_excl5()
            setTimeout(function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $(this.s5ctrl.step).offset().top
                }, 10)
            }.bind(this), 300)
        }.bind(this), 10)
    }

    s4onclick() {
        consolelog("FormUIController/s4onclick", "Called!")
        this.s4ctrl.updatelabel()
        this.reservations.createrecursivereservation()
        this.disableall_excl5()
    }

    undisable_5() {
        consolelog("FormUIController/undisable_5", "Undisabling S5")
        this.s5ctrl.disable(false)
        $(this.s5step).removeClass("disabled")
    }

    disableall_excl5() {
        consolelog("FormUIController/disableall_excl5", "Disabling all except S5")
        this.s1ctrl.disable(true)
        this.s2ctrl.disable(true)
        this.s3ctrl.disable(true)
        this.s4ctrl.disable(true)
        this.s5ctrl.disable(false)
    }

    undisableall() {
        consolelog("FormUIController/undisableall", "Undisabling all!")
        this.s1ctrl.disable(false)
        this.s2ctrl.disable(false)
        this.s3ctrl.disable(false)
        this.s4ctrl.disable(false)
        this.s5ctrl.disable(false)
    }

    disable_5() {
        consolelog("FormUIController/disable_5", "Disabling Step 5")
        this.s5ctrl.disable(true)
    }

    clearlabels() {
        consolelog("FormUIController/clearlabels", "Clearing all labels!")
        this.s1ctrl.updatelabel("")
        this.s2ctrl.updatelabel("")
        this.s3ctrl.updatelabel("")
        this.s4ctrl.updatelabel("")
        $(this.s2ctrl.step).removeClass("done")
        $(this.s3ctrl.step).removeClass("done")
        $(this.s4ctrl.step).removeClass("done")
        $(this.s5ctrl.step).removeClass("done")
    }

    clearinputs() {
        consolelog("FormUIController/clearinputs", "Clearing all inputs!")
        $("input[name=" + this.s1ctrl.formname + "]").prop("checked", false)
        $(this.s2ctrl.input).val("")
        $(this.s3ctrl.input).val("")
        $(this.s4ctrl.timesinput).val("1")
        $("input[name=" + this.s4ctrl.downame).prop("checked", true)
    }

    backtostart() {
        consolelog("FormUIController/backtostart", "Going back to start!")
        this.undisableall()
        setTimeout(function () {
            $(this.stepper).nextStep()
            $(this.stepper).nextStep()
            this.clearlabels()
            this.clearinputs()
            this.disable_5()
            setTimeout(function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $(this.s1ctrl.step).offset().top
                }, 10)
            }.bind(this), 300)
        }.bind(this), 10)

    }
}class DateUI {
    constructor(label, step, content, input, invalidfeedback) {
        this.label = label
        this.step = step
        this.content = content
        this.input = input
        this.invalidfeedback = invalidfeedback
        consolelog("DateUI", "A new Date UI object is being created.")
    }

    autodate(offset) {
        var timestamp = (new Date().getTime() / 1000) + offset
        var adjusted = Math.round(timestamp / 1800)
        var finalts = adjusted * 1800
        var momentobj = moment.unix(finalts)
        consolelog("DateUI/autodate", "Autodate activated with offset " + offset + ", end result " + momentobj.format("MM/DD/YYYY"))
        $(this.input).val(momentobj.format("MM/DD/YYYY"))
    }

    validate(onclick) {
        consolelog("DateUI/validate", "Validating...")
        if (onclick) {
            if (!$(this.content).is(":visible")) {
                consolelog("DateUI/validate", "Validate onclick failed, content not visible.")
                return false
            }
        }

        if (moment($(this.input).val(), "MM/DD/YYYY").isValid()) {
            $(this.step).removeClass("invalid")
            this.updatelabel()
            $(this.input).removeClass("invalid").addClass("valid")
            $(this.invalidfeedback).hide()
            consolelog("DateUI/validate", "Non-onclick validation success")
            return true
        } else {
            if ($(this.input).val().includes("poopoo") && $(this.input).val().includes("peepee")) {
                $(this.invalidfeedback).html("haha hehe")
            } else if ($(this.input).val().includes(";)")) {
                $(this.invalidfeedback).html("*bonk* Go to horny jail! Now!")
            } else if ($(this.input).val().includes("fuck you")) {
                $(this.invalidfeedback).html("Oh fuck off will you")
            } else {
                $(this.invalidfeedback).html("Please enter a valid date.")
            }
            $(this.step).addClass("wrong")
            this.updatelabel("")
            $(this.input).addClass("invalid")
            $(this.invalidfeedback).show()
            var tempval = $(this.input).val()
            // This is the magic code that prevents going on to Step 3 onclick.
            // Basically clears the value so that the form thinks you need to enter a value. But
            // we put back the user's input 1ms later.

            // CSS magic by turning down the opacity of the default label feedback to 0 also makes it so that we 100%
            // use our custom validation.
            if (onclick) {
                $(this.input).val("")
            }
            setTimeout(function (val) {
                $(this.input).val(val)
            }.bind(this, tempval), 1)
            if (clippymode) {
                clippyagent.stop()
                clippyagent.speak("It seems like you're having trouble selecting a date. Try picking today!")
            }
            consolelog("DateUI/validate", "Non-onclick validation failure")
            return false
        }
    }

    updatelabel(string) {
        if (string == undefined) {
            $(this.label).attr("data-step-label", $(this.input).val())
            consolelog("DateUI/updatelabel", "Label is being updated to " + $(this.input).val())
        } else {
            $(this.label).attr("data-step-label", string)
            consolelog("DateUI/updatelabel", "Label is being updated to " + string)
        }
    }

    disable(state) {
        consolelog("DateUI/disable", "DateUI disable state: " + state)
        if (state) {
            $(this.step).addClass("disabled")
        } else {
            $(this.step).removeClass("disabled")
        }
    }
}class ReoccurUI {
    constructor (label, step, content, timesinput, downame) {
        this.label = label
        this.step = step
        this.content = content
        this.timesinput = timesinput
        this.downame = downame
        consolelog("ReoccurUI", "New object created")
    }

    updatelabel(string) {
        if (string == undefined) {
            var labeltext = ""
            labeltext = $(this.timesinput).val() == "1" ? "1 reservation" : $(this.timesinput).val() + " reservations"
            if ($(this.timesinput).val() != "1") {
                var dayspicked = 0
                $('input[name=' + this.downame + ']').each(function () {
                    this.checked ? dayspicked = dayspicked + 1 : false
                });
                if (dayspicked != 7) {
                    labeltext = labeltext + ", reoccurring on "
                    $('input[name=' + this.downame + ']').each(function () {
                        var sThisVal = (this.checked ? $(this).val() : "");
                        if (dow[sThisVal] != undefined) {
                            labeltext = labeltext + dow[sThisVal].substring(0, 3) + ", "
                        }
                    });
                    labeltext = labeltext.substring(0, labeltext.length - 2)
                } else {
                    labeltext = labeltext + ", reoccurring every day"
                }
            }
            $(this.label).attr("data-step-label", labeltext)
            consolelog("ReoccurUI/updatelabel", "Label being updated to " + labeltext)
        } else {
            $(this.label).attr("data-step-label", string)
            consolelog("ReoccurUI/updatelabel", "Label is now " + string)
        }
    }

    disable(state) {
        consolelog("ReoccurUI/disable", "Disabling with state: " + state)
        if (state) {
            $(this.step).addClass("disabled")
        } else {
            $(this.step).removeClass("disabled")
        }
    }
}class TimeUI {
    constructor (label, step, content, input, invalidfeedback) {
        this.label = label
        this.step = step
        this.content = content
        this.input = input
        this.invalidfeedback = invalidfeedback
        $(this.input).change(this.updatetime.bind(this))
        consolelog("TimeUI", "Created new Time UI object.")
    }

    autotime(offset) {
        var timestamp = (new Date().getTime() / 1000) + (offset * 60)
        var adjusted = Math.round(timestamp / 1800)
        var finalts = adjusted * 1800
        var momentobj = moment.unix(finalts)
        $(this.input).val(momentobj.format("h:mm A"))
        consolelog("TimeUI/autotime", "Auto time created with offset " + offset + " and result " + momentobj.format("h:mm A"))
    }

    changetime(offset) {
        if ($(this.input).val() == "") {
            this.autotime(offset)
            return
        }
        var momentobj = moment($(this.input).val(), "h:mm A")
        var currentdate = momentobj.date()

        momentobj.add(offset, "minutes")
        if (momentobj.date() == currentdate) {
            $(this.input).val(momentobj.format("h:mm A"))
            consolelog("TimeUI/changetime", "Change time success with offset " + offset + " and result " + momentobj.format("h:mm A"))
        }
    }

    updatetime() {
        var text = $(this.input).val()
        // Matches if the length of the text is 7 (always will be), has an AM/PM, and has zero spaces.
        // I would prefer not having to do this, but the included stuff doesn't work :/
        if (text.length != 7 || !(text.includes("AM") || text.includes("PM")) || text.includes(" ")) {
            consolelog("TimeUI/updatetime", "Time label formatting is not being updated. Conditions not met.")
            return
        }

        if (text.charAt(0) == "0") {
            text = text.substring(1)
        }

        // Ternary operators mean this is super clean.
        consolelog("TimeUI/updatetime", "Time label formatting is being updated.")
        $(this.input).val(text.split("A").length == 2 ? text = text.split("A")[0] + " AM" : text = text.split("P")[0] + " PM")
    }

    validate(onclick) {
        if (onclick) {
            if (!$(this.content).is(":visible")) {
                consolelog("TimeUI/validate", "Validation on-click failing, content is not visible.")
                return false
            }
        }

        if (moment($(this.input).val(), "h:mm A").isValid()) {
            $(this.step).removeClass("invalid")
            this.updatelabel()
            $(this.input).removeClass("invalid").addClass("valid")
            $(this.invalidfeedback).hide()
            consolelog("TimeUI/validate", "Input validation success!")
            return true
        } else {
            $(this.invalidfeedback).html("Please enter a valid time.")
            $(this.step).addClass("wrong")
            this.updatelabel("")
            $(this.input).addClass("invalid")
            $(this.invalidfeedback).show()
            var tempval = $(this.input).val()
            // This is the magic code that prevents going on to Step 3 onclick.
            // Basically clears the value so that the form thinks you need to enter a value. But
            // we put back the user's input 1ms later.

            // CSS magic by turning down the opacity of the default label feedback to 0 also makes it so that we 100%
            // use our custom validation.
            if (onclick) {
                $(this.input).val("")
            }
            setTimeout(function (val) {
                $(this.input).val(val)
            }.bind(this, tempval), 1)
            if (clippymode) {
                clippyagent.stop()
                if (Math.random() * 10 <= 5) {
                    clippyagent.speak("It seems like you're having trouble entering a time. Make sure you include the hours and the minutes!")
                } else {
                    clippyagent.speak("It seems like you're having trouble entering a time. Try something like 4:20 PM.")
                } 
            }
            consolelog("TimeUI/validate", "Input validation failure.")
            return false
        }
    }

    updatelabel(string) {
        if (string == undefined) {
            $(this.label).attr("data-step-label", $(this.input).val())
            consolelog("TimeUI/updatelabel", "Label is being updated to " + $(this.input).val())
        } else {
            $(this.label).attr("data-step-label", string)
            consolelog("TimeUI/updatelabel", "Label updated to " + string)
        }
    }

    disable(state) {
        consolelog("TimeUI/disable", "Disabling with state: " + state)
        if (state) {
            $(this.step).addClass("disabled")
        } else {
            $(this.step).removeClass("disabled")
        }
    }
}class ConfirmUI {
    constructor (label, step, content, reservcount, collapse, tablebody) {
        this.label = label
        this.step = step
        this.content = content
        this.reservcount = reservcount
        this.collapse = collapse
        this.tablebody = tablebody
        consolelog("ConfirmUI", "A new confirm UI object is being created.")
    }

    updatelabel(string) {
        if (string != undefined) {
            $(this.label).attr("data-step-label", string)
            $(this.reservcount).html(string)
            consolelog("ConfirmUI/updatelabel", "Label updating to " + string)
        }
        consolelog("ConfirmUI/updatelabel", "Label is being updated to " + string)
    }

    updatetable(reservations, collapse) {
        consolelog("ConfirmUI/updatetable", "Table is being updated with " + reservations.reservations.length + " reservations, collapse set to " + collapse)
        $(this.tablebody).empty()
        for (var i = 0; i < reservations.reservations.length; i++) {
            var momentobj = reservations.reservations[i].momentobj
            var location = locations.getlocationname(reservations.reservations[i].location)
            var date = momentobj.format("LL")
            var time = momentobj.format("LT")
            $(this.tablebody).append("<tr><td>" + date + "</td><td>" + time + "</td><td>" + location + "</td><td><a class='btn btn-sm btn-danger waves-effect' onclick='reservations.deletereservation(" + i + ")'><i class='fas fa-trash'></i></a></td>")
        }
        if (collapse) {
            if (reservations.reservations.length <= 5) {
                $(this.collapse).collapse("show")
            } else {
                $(this.collapse).collapse("hide")
            }
        }
    }

    collapsechange(state) {
        consolelog("ConfirmUI/collapsechange", "Collapse state being set to " + state)
        if (state) {
            $(this.collapse).collapse("show")
        } else {
            $(this.collapse).collapse("hide")
        }
    }

    disable(state) {
        consolelog("ConfirmUI/disable", "ConfirmUI step disabling: " + state)
        if (state) {
            $(this.step).addClass("disabled")
        } else {
            $(this.step).removeClass("disabled")
        }
    }
}class LocationUI {
    constructor (label, step, content, formname, nextui, feedback) {
        this.label = label
        this.step = step
        this.content = content
        this.formname = formname
        this.nextui = nextui
        this.feedback = feedback
        document.addEventListener(eventctrl.locationSelected.type, this.validate.bind(this))
        consolelog("LocationUI", "New LocationUI object is being constructed.")
    }

    validate() {
        if ($("input[name=" + this.formname + "]:checked").val() != undefined) {
            $(this.step).removeClass("wrong")
            $(this.feedback).hide()
            consolelog("LocationUI/validate", "Good validation.")
            return true
        } else {
            $(this.step).addClass("wrong")
            $(this.feedback).show()
            if (clippymode) {
                if (Math.random() * 10 <= 5) {
                    clippyagent.stop()
                    clippyagent.speak("It seems like you're having trouble selecting a location. Have you thought about Morgan Dining Hall?")
                } else {
                    clippyagent.stop()
                    clippyagent.speak("It seems like you're having trouble selecting a location. Have you considered the Campus Center Food Court?")
                }
            }
            consolelog("LocationUI/validate", "Bad validation.")
            return false
        }
    }

    updatelabel(string, onclick) {
        if (string == undefined) {
            if (onclick) {
                this.validate()
            }
            setTimeout(function() {
                if ($(this.nextui.content).is(":visible")) {
                    var location = $("input[name=" + this.formname + "]:checked").val()
                    consolelog("LocationUI/updatelabel", "Updating label to " + locations.getlocationname(location))
                    $(this.label).attr("data-step-label", locations.getlocationname(location));
                }
            }.bind(this), 10)
        } else {
            consolelog("LocationUI/updatelabel", "Updating label to " + string)
            $(this.label).attr("data-step-label", string)
        }
    }

    disable(state) {
        consolelog("LocationUI/disable", "Disabling with state: " + state)
        if (state) {
            $(this.step).addClass("disabled")
        } else {
            $(this.step).removeClass("disabled")
        }
    }
}class SubmissionUI {
    constructor (madelabel, totallabel) {
        this.madelabel = madelabel
        this.totallabel = totallabel
        consolelog("SubmissionUI", "Created new submission UI object.")
    }

    updatemadelabel(string) {
        $(this.madelabel).html(string)
        consolelog("SubmissionUI/updatemadelabel", "Updated made label to " + string)
    }

    updatetotallabel(string) {
        $(this.totallabel).html(string)
        consolelog("SubmissionUI/updatetotallabel", "Updated total label to " + string)
    }
}/**
 * Class to hold all the reservations, which is just a bunch of Reservation.
 */
class Reservations {
    // To be constructed
    constructor(s1ctrl, s2ctrl, s3ctrl, s4ctrl, s5ctrl, s6ctrl, auth, confirmbutton) {
        this.reservations = []
        this.s1ctrl = s1ctrl
        this.s2ctrl = s2ctrl
        this.s3ctrl = s3ctrl
        this.s4ctrl = s4ctrl
        this.s5ctrl = s5ctrl
        this.s6ctrl = s6ctrl
        this.auth = auth
        this.confirmbutton = confirmbutton
        $(this.confirmbutton).hide()
        document.addEventListener(eventctrl.makeReservations.type, this.makereservations.bind(this, 0))
        document.addEventListener(eventctrl.deletePendingReservations.type, this.deleteallreservations.bind(this))
        consolelog("Reservations", "Created new reservations object.")
    }
    
    deletereservation(index) {
        consolelog("Reservations/deletereservation", "Deleting reservation at index " + index)
        this.reservations.splice(index, 1)
        this.s5ctrl.updatetable(this)
        this.reservations.length == 1 ? this.s5ctrl.updatelabel("1 reservation") : this.s5ctrl.updatelabel(this.reservations.length + " reservations")
        if (this.reservations.length == 0) {
            consolelog("Reservations/deletereservation", "0 reservations exist, going back to start of form")
            $(this.confirmbutton).hide()
            if ($(this.s5ctrl.content).is(":visible")) {
                eventctrl.emit(eventctrl.backToStart)
            }
        }
    }

    deleteallreservations() {
        consolelog("Reservations/deleteallreservations", "Deleting all reservations!")
        var totallength = this.reservations.length
        for (var i = 0; i < totallength; i++) {
            this.deletereservation(0)
        }
    }
    // Method that takes in a unix timestamp and shoves out a reservation into the array. No complicated stuff needed.
    createreservation(timestamp, location, collapse) {
        $(this.confirmbutton).show()
        var momentobj = moment.unix(timestamp)
        var reserv = new Reservation(momentobj.format("YYYY-MM-DD"), momentobj.hour(), momentobj.minute(), location, momentobj)
        consolelog("Reservations/createreservation", "Created new reservation: from timestamp " + timestamp + " location " + location)
        this.reservations.push(reserv)
        this.reservations.length == 1 ? this.s5ctrl.updatelabel("1 reservation") : this.s5ctrl.updatelabel(this.reservations.length + " reservations")
        this.s5ctrl.updatetable(this, collapse)
    }
    
    createrecursivereservation() {
        consolelog("Reservations/createrecursivereservation", "Now creating recursive reservations.")
        this.createsinglereservation(false)
        var basemoment = moment($(this.s2ctrl.input).val() + " " + $(this.s3ctrl.input).val(), "M/D/YYYY h:mm A")
        var basets = basemoment.unix()
        var acceptabledows = []
        $('input[name=' + this.s4ctrl.downame + ']').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal != undefined) {
                acceptabledows.push(sThisVal)
            }
        });
        for (var i = 0; i < $(this.s4ctrl.timesinput).val() - 1;) {
            basets = basets + 86400
            var modifiedmoment = moment.unix(basets)
            if (acceptabledows.includes(modifiedmoment.day().toString())) {
                this.createreservation(basets, $("input[name=" + this.s1ctrl.formname + "]:checked").val(), true)
                i++
            }
        }
        if (this.reservations.length > 5) {
            setTimeout(function () {
                this.s5ctrl.collapsechange(false)
            }.bind(this), 20)
        }
    }

    makereservations(index) {
        if (index == undefined) {
            index = 0
        }
        this.s6ctrl.updatemadelabel(index)
        this.s6ctrl.updatetotallabel(this.reservations.length)
        var reservobj = this.reservations[index]
        consolelog("Reservations/makereservations", "Making reservation at index " + index)
        var url = "https://api.dineoncampus.com/v1/reservations"
        // The local user ID is being a bit wonky after login, so just get it every time.
        var data = {
            "customer_id": localStorage.getItem("userid"),
            "date": reservobj.date,
            "friends": 0,
            "hour": reservobj.hour,
            "location_id": reservobj.location,
            "minute": reservobj.minute
        }
        var xhr = new XHRwrap(url, "POST", true, 30000, data, this.makereservations_callback.bind(this))
        xhr.request()
    }

    makereservations_callback(request) {
        var data = request.responseText
        try { 
            var obj = JSON.parse(data)
            var status = obj['status']
        } catch (error) {
            consolelog("Reservations/makereservations_callback", "Failed to make reservation (JSON parse). Failing whole process.")
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("I'm sorry I couldn't make your dining reservations. Have you tried redialing into AOL?")
            }
            eventctrl.emit(eventctrl.makeReservationsFail)
            return
        }

        if (obj['status'] != "success") {
            consolelog("Reservations/makereservations_callback", "Status for reservation creation is NOT success. Failing whole process.")
            eventctrl.emit(eventctrl.makeReservationsFail)
        } else {
            var curindex = $(this.s6ctrl.madelabel).html()
            var total = $(this.s6ctrl.totallabel).html()
            if (parseInt(curindex) + 1 == parseInt(total)) {
                this.s6ctrl.updatemadelabel(parseInt(curindex) + 1)
                setTimeout(function () {
                    eventctrl.emit(eventctrl.makeReservationsSuccess)
                    if (clippymode) {
                        clippyagent.stop()
                        clippyagent.play("Congratulate")
                        clippyagent.speak("I'm so glad I could help make your dining reservations. Enjoy your meal!")
                        clearInterval(clippyinterval)
                    }
                }.bind(this), 500)
            } else {
                this.makereservations(parseInt(curindex) + 1)
            }
        }
        
    }

    createsinglereservation(collapsedeny) {
        consolelog("Reservations/createsinglereservation", "Creating single reservation with collapsedeny: " + collapsedeny)
        var momentobj = moment($(this.s2ctrl.input).val() + " " + $(this.s3ctrl.input).val(), "M/D/YYYY h:mm A")
        if (collapsedeny) {
            this.createreservation(momentobj.unix(), $("input[name=" + this.s1ctrl.formname + "]:checked").val(), false)
        } else {
            this.createreservation(momentobj.unix(), $("input[name=" + this.s1ctrl.formname + "]:checked").val(), true)
        }
    }
}class Reservation {
    constructor(date, hour, minute, location, momentobj) {
        this.date = date
        this.hour = hour
        this.minute = minute
        this.location = location
        this.momentobj = momentobj
    }
}class DeleteReservation {
    constructor (auth, remaininglabel, totallabel, includefuture) {
        this.auth = auth
        this.remaininglabel = remaininglabel
        this.totallabel = totallabel
        this.includefuture = includefuture
        var url = "https://api.dineoncampus.com/v1/customer/reservations?customer_id=" + this.auth.userid
        var xhr = new XHRwrap(url, "GET", true, 30000, null, this.reservlist_callback.bind(this))
        xhr.request()
        eventctrl.emit(eventctrl.deleteReservationsInit)
        document.addEventListener(eventctrl.deleteReservationsP1SpamConfirm.type, this.deletereservation.bind(this, 0))
        consolelog("DeleteReservation", "Beginning deleting reservations.")
    }

    reservlist_callback(response) {
        var data = response.responseText
        try {
            var obj = JSON.parse(data)
            var status = obj['status']
        } catch (error) {
            consolelog("DeleteReservation/reservlist_callback", "Failed Phase 1, JSON parse error")
            eventctrl.emit(eventctrl.deleteReservationsP1Fail)
            return
        }

        if (obj['status'] == "success") {
            this.reservationids = []
            for (var i = 0; i < obj['reservations'].length; i++) {
                if (this.includefuture == false) {
                    var reserv_moment = moment(obj['reservations'][i]['date'])
                    reserv_moment.subtract(3, "hours")
                    reserv_moment.hour(obj['reservations'][i]['hour'])
                    reserv_moment.minute(obj['reservations'][i]['minute'])
                    var current_moment = moment()
                    if (current_moment.diff(reserv_moment) >= 7200000) {
                        this.reservationids.push(obj['reservations'][i]['id'])
                    }
                } else {
                    this.reservationids.push(obj['reservations'][i]['id'])
                }
            }
            consolelog("DeleteReservation/reservlist_callback", "Found " + this.reservationids.length + " candidates for deletion")
            if (this.reservationids.length == 0) {
                eventctrl.emit(eventctrl.deleteReservationsP1None)
                if (this.includefuture == false) {
                    $(".deleteall-pastspan").show()
                }
                return
            } else if (this.reservationids.length >= 20) {
                consolelog("DeleteReservation/reservlist_callback", "User has 20+ reservations, confirming...")
                setTimeout(function () {
                    eventctrl.emit(eventctrl.deleteReservationsP1SpamWarn)
                    $(".s6-deleteall-spam-number").html(this.reservationids.length)
                }.bind(this), 500)
                return
            }
            eventctrl.emit(eventctrl.deleteReservationsP1Success)
            this.deletereservation(0)
        } else {
            consolelog("DeleteReservation/reservlist_callback", "Request status was not success, failing at Phase 1")
            eventctrl.emit(eventctrl.deleteReservationsP1Fail)
            return
        }
    }

    deletereservation(index) {
        if (index == undefined) {
            index = 0
        }
        consolelog("DeleteReservation/deletereservation", "Deleting reservation at index " + index)
        $(this.remaininglabel).html(index)
        $(this.totallabel).html(this.reservationids.length)
        var id = this.reservationids[index]
        var url = "https://api.dineoncampus.com/v1/reservations/?reservation_id=" + id + "&customer_id=" + this.auth.userid
        var xhr = new XHRwrap(url, "DELETE", true, 30000, null, this.deletereservation_callback.bind(this))
        xhr.request()
    }

    deletereservation_callback(request) {
        var data = request.responseText
        try { 
            var obj = JSON.parse(data)
            var status = obj['status']
        } catch (error) {
            eventctrl.emit(eventctrl.deleteReservationsFail)
            consolelog("DeleteReservation/deletereservation_callback", "Failed to confirm delete reservation at JSON parse, failing process.")
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("I'm sorry I couldn't delete your reservations. Make sure your connection to AOL is strong!")
            }
            return
        }

        if (obj['status'] != "success") {
            eventctrl.emit(eventctrl.deleteReservationsFail)
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("I'm sorry I couldn't delete your reservations. Make sure your connection to AOL is strong!")
            }
            consolelog("DeleteReservation/deletereservation_callback", "Delete reservation was not successful, failing process.")
            return
        } else {
            var curindex = $(this.remaininglabel).html()
            var total = $(this.totallabel).html()
            if (parseInt(curindex) + 1 == parseInt(total)) {
                this.remaininglabel.html(parseInt(curindex) + 1)
                consolelog("DeleteReservation/deletereservation_callback", "No more reservations to delete, finishing process.")
                setTimeout(function () {
                    eventctrl.emit(eventctrl.deleteReservationsSuccess)
                    if (clippymode) {
                        clippyagent.stop()
                        clippyagent.play("Congratulate")
                        clippyagent.speak("I'm so glad I could help delete some reservations. Enjoy your email spam!")
                    }
                }.bind(this), 500)

            } else {
                this.deletereservation(parseInt(curindex) + 1)
            }
        }
    }
}/* eslint-disable no-delete-var */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
var uictrl;
var auth;
var locations;
var frmctrl;
var s1ctrl;
var s2ctrl;
var s3ctrl;
var s4ctrl;
var s5ctrl;
var s6ctrl;
var eatctrl;
var dow = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
var reservations;
var appearance;
var dakamode;
var clippyagent;
var clippyinterval;
var clippymode;

console.log("%cHey! Welcome to DOCBatch!", "font-size: 24px")
console.log("%cIf you're wondering how DOCBatch works behind the scenes, there's lots of internal logging about the operations of DOCBatch.", "font-size: 18px")
console.log("%cEnjoy!", "font-size: 18px")

// https://stackoverflow.com/a/901144
function getParameterByName(name, url = window.location.href) {
  if (!url) url = window.location.href;
  name = name.replace(/[[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var browser = bowser.getParser(window.navigator.userAgent);
var plattype = browser.getPlatformType()
consolelog("onload", "Platform type is " + plattype)
$(function () {
  consolelog("onload", "DOM has loaded!")
    $('.stepper').mdbStepper();
    $("#deleteall-checkbox").prop("checked", false)

    if (plattype != "desktop") {
      consolelog("onload", "Platform type is not desktop, hiding login instructions")
      $("#uid-desktop-instructions").hide()
      $("#uid-notdesktop").show()
    }

    if (localStorage.getItem("appearance") == null) {
      localStorage.setItem("appearance", "automatic")
    }

    if (localStorage.getItem("clippymode") == null) {
      localStorage.setItem("clippymode", "false")
    }

    if (localStorage.getItem("clippymode") == "false") {
      clippymode = false
    } else if (localStorage.getItem("clippymode") == "true") {
      clippymode = true
    }

    if (clippymode) {
      clippy.load("Clippy", function (agent) {
        clippyagent = agent;
      })

      $("#clippymode").html("On")
    }


    var unixtime = new Date().getTime() / 1000
    if (unixtime > 1620878400) {
      $("#db3-shutdown-danger").show()
      db3shutdownupdate()
      setInterval(db3shutdownupdate, 60000)
    } else {
      if (localStorage.getItem("db3warndismiss") != "true") {
        $("#db3-shutdown-warning").show()
      }
    }

    initDOCBatch(true)

    $('.datepicker').datepicker({
      format: "mm/dd/yyyy",
      today: "Today",
      clear: "Clear",
      close: "Close"
    });

    //$(".timepicker").clockpicker({
    //  afterDone: function() {
    //    console.log("Done!")
    //  }
    //})
    if (unixtime > 1621278000) {
      eventctrl.emit(eventctrl.shutdownFade)
      return
    } else {
      auth.local_login()
    }
    
    document.addEventListener(eventctrl.successLoad.type, function () {
      if (clippymode) {
        console.log("Clippy go!")
        setTimeout(function () {
          clippyagent.show()
          if (window.innerWidth >= 600) {
            clippyagent.moveTo(window.innerWidth * 0.85, window.innerHeight * 0.85)
          } else {
            clippyagent.moveTo(window.innerWidth * 0.50, window.innerHeight * 0.85)
          }
          clippyagent.speak("Welcome to DOCBatch! I'm Clippy, and I'm here to help you make dining reservations.")
        }, 1000)

        clippyinterval = setInterval(function() {
          console.log("Clippy animate!")
          clippyagent.animate()
        }, 15000)
      }
    })
})

function initDOCBatch(fromdom) {
  consolelog("initDOCBatch", "DOCBatch is being init'd")
  
  appearance = new Appearance("static/css/dark.css", $("#floating-appearance"), $("#basehtml"))
  appearance.init(true)
  
  initdakamode()

  if (fromdom) {
    $("#loader").show()
  }
  uictrl = new UIController($("#loader"), $("#mainsteps"), $("#login"), $("#step6"), $("#failedload"), $("#step6-success"), $("#step6-fail"), $("#s6-deleteall-p1"), $("#s6-deleteall-p1fail"), $("#s6-deleteall-p1none"), $("#s6-deleteall"), $("#s6-deleteall-fail"), $("#s6-deleteall-success"), $("#login-userid"), $("#eatnow-locating"), $("#eatnow-failed"), $("#eatnow-nothing"), $("#s6-deleteall-spam"), $("#aprilfools"), $("#shutdown"))
  auth = new Authentication("userid", $("#login-email"), $("#login-password"), $("#login-submit"), $("#login-userid-field"), $("#login-submit-userid"))
  locations = new Locations("https://api.docbatch.app/api/v1/locations", $(".fl-ec"), $(".fl-expl"), $("#locations-form"), "radiocopy")
  s2ctrl = new DateUI($("#s2step-label"), $("#step2"), $("#s2content"), $("#s2-datepick"), $("#s2-invalidfeedback"))
  s1ctrl = new LocationUI($("#s1step-label"), $("#step1"), ("#s1content"), "locations-form", s2ctrl, $("#s1-invalidfeedback"))
  s3ctrl = new TimeUI($("#s3step-label"), $("#step3"), $("#s3content"), $("#s3-timepick"), $("#s3-invalidfeedback"))
  s4ctrl = new ReoccurUI($("#s4step-label"), $("#step4"), $("#s4content"), $("#s4-reoccurpick"), "s4-datepicker")
  s5ctrl = new ConfirmUI($("#s5step-label"), $("#step5"), $("#s5content"), $("#s5-reservcount"), $("#s5-reservationtable"), $("#s5-currentreservations-body"))
  s6ctrl = new SubmissionUI($("#s6-remaining"), $("#s6-total"))
  reservations = new Reservations(s1ctrl, s2ctrl, s3ctrl, s4ctrl, s5ctrl, s6ctrl, auth, $("#s1-skiptos5"))
  frmctrl = new FormUIController(s1ctrl, s2ctrl, s3ctrl, s4ctrl, s5ctrl, $("#feedback-step"), reservations,  locations, $("#rfnclosedModal"))
  eatctrl = new EatNow(s1ctrl, frmctrl, locations)
  frmctrl.clearinputs()
  consolelog("initDOCBatch", "DOCBatch has been initalized!")
}

function testingtesting() {
  console.log("Test!")
}

function initdakamode() {
  if (localStorage.getItem("dakamode") == null) {
    localStorage.setItem("dakamode", "false")
  }

  if (localStorage.getItem("dakamode") == "true") {
    dakamode = true
  } else if (localStorage.getItem("dakamode") == "false") {
    dakamode = false
  }

  if (dakamode) {
    $(function () {
      $(".dakamode").show()
      $(".notdakamode").hide()
      $("#dakamode").html("On")
    })
  }
}

function dakatoggle() {
  if (localStorage.getItem("dakamode") == "false") {
    localStorage.setItem("dakamode", "true")
  } else if (localStorage.getItem("dakamode") == "true") {
    localStorage.setItem("dakamode", "false")
  }

  window.location.reload()
}

function clippytoggle() {
  if (localStorage.getItem("clippymode") == "false") {
    localStorage.setItem("clippymode", "true")
  } else if (localStorage.getItem("clippymode") == "true") {
    localStorage.setItem("clippymode", "false")
  }

  window.location.reload()
}

function hm_tostring(hour, minute) {
  var denom;
  if (hour >= 12) {
    denom = "PM"
  } else {
    denom = "AM"
  }
  if (hour >= 13) {
    hour = hour - 12
  } else if (hour == 0) {
    hour = 12
  }

  if (minute <= 9) {
    minute = "0" + minute
  }

  return hour + ":" + minute + " " + denom
}

function deleteAll() {
  consolelog("deleteAll", "Deleting all reservations.")
  deleteall = new DeleteReservation(auth, $("#s6-delall-remaining"), $("#s6-delall-total"), $("#deleteall-checkbox").is(":checked"))
}

function fillUID() {
  consolelog("fillUID", "Filling UID (wmi-span)")
  $("#wmi-span").html(localStorage.getItem("userid"))
}

function db3warndismiss() {
  localStorage.setItem("db3warndismiss", "true")
}

function db3shutdownupdate() {
  var calculated_duration = 1621278000000 - new Date().getTime()
  var moment_duration = moment.duration(calculated_duration)
  $("#db3-shutdown-duration").html(moment_duration.humanize(true, {h: 97}))
}

function clearDelete() {
  $("#deleteall-checkbox").prop("checked", false)
}class EatNow {
    constructor (s1ctrl, frmctrl, locations) {
        this.s1ctrl = s1ctrl
        this.frmctrl = frmctrl
        this.locations = locations
        consolelog("EatNow", "A new Eat Now object has been created.")
    }

    eatnow() {
        consolelog("EatNow/eatnow", "Eat Now called, waiting 100ms for transition to finish")
        eventctrl.emit(eventctrl.eatNowStart)
        setTimeout(function () {
            this.getlocation()
        }.bind(this), 100)
    }

    getlocation() {
        var options = {
            enableHighAccuracy: true,
            maximumAge: 0,
            timeout: 30000
        }
        navigator.geolocation.getCurrentPosition(this.getlocation_success.bind(this), this.getlocation_fail.bind(this), options)
        consolelog("EatNow/getlocation", "geolocation request went through")
    }

    getlocation_success(position) {
        var latitude = position.coords.latitude
        var longitude = position.coords.longitude
        consolelog("EatNow/getlocation_success", "Geolocation good, user location is " + latitude + " " + longitude)
        var locarray = this.locations.getlocations()
        var closestid;
        var closestid_distance = 99999;
        var location_closestid;
        var location_distance = 99999;
        console.group("%c[EatNow/getlocation_success]%c Parsing locations", "color: purple, font-weight: bold", "")
        Object.keys(locarray).forEach(function (k) {
            consolelog("EatNow/getlocation_success", "Parsing for " + k)
            if (k == "5f344f0b0101560dbd3cb05b") {
                consolelog("EatNow/getlocation_success", "Is on blacklist, continuing...")
                return
            }

            if (locarray[k]['reservations'] == false) {
                consolelog("EatNow/getlocation_success", "Does not accept reservations, continuing...")
                return
            }
            var opstatus = this.locations.getoperationstatus(k)
            var distance = this.haversineCalc(locarray[k]['lat'], locarray[k]['lng'], latitude, longitude)
            if (opstatus['status'] == "closed") {
                consolelog("EatNow/getlocation_success", "Location is closed!")
                if (distance < location_distance) {
                    location_closestid = k
                    location_distance = distance
                }
                return
            }

            if (distance <= 1.0 && distance < closestid_distance) {
                consolelog("EatNow/getlocation_success", "Closest location found! Distance is " + distance + " (compared to dist of " + closestid_distance + ")")
                closestid = k
                closestid_distance = distance
            } else {
                consolelog("EatNow/getlocation_success", "Location is further than 1 mile away (distance = " + distance + ")")
                if (distance < location_distance) {
                    consolelog("EatNow/getlocation_success", "Closest location found! Distance is " + distance + " (compared to dist of " + closestid_distance + ")")
                    location_closestid = k
                    location_distance = distance
                }
            }
        }.bind(this))
        console.groupEnd()

        if (closestid != undefined) {
            consolelog("EatNow/getlocation_success", "Found a closest location with id " + closestid)
            eventctrl.emit(eventctrl.eatNowSuccess)
            $("input[name='" + this.s1ctrl.formname + "'][value='" + closestid + "']").prop("checked", true)
            this.frmctrl.reservefornow(true)
            $("#eatnow-nothing-expl-normal").show()
            $("#eatnow-nothing-expl-toofar").hide()
        } else {
            consolelog("EatNow/getlocation_success", "Failed to find closest location...")
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("I'm sorry I couldn't find any locations for you to eat now at.")
            }
            $("#eatnow-nothing-expl-normal").show()
            $("#eatnow-nothing-expl-toofar").hide()
            if (location_distance > 1.0) {
                consolelog("EatNow/getlocation_success", "...Because everything was too far away (nearest = " + location_distance + ")")
                $("#eatnow-nothing-expl-normal").hide()
                $("#eatnow-nothing-expl-toofar").show()
                $("#eatnow-nothing-exp-toofar-1").html(this.locations.getlocationname(location_closestid))
                $("#eatnow-nothing-exp-toofar-2").html(location_distance.toFixed(2) + " miles ")
            }
            eventctrl.emit(eventctrl.eatNowNothing)
        }
    }

    getlocation_fail(error) {
        consolelog("EatNow/getlocation_fail", "Failed to get location with EC " + error.code)
        $("#eatnow-failed-code1").hide()
        $("#eatnow-failed-code2").hide()
        $("#eatnow-failed-code3").hide()
        $("#eatnow-failed-generic").hide()
        if (error.code == 1) {
            $("#eatnow-failed-code1").show()
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("You seem to be having trouble understanding that I need to know your location so I can find where you can eat. Have you tried getting some more brain cells?")
            }
        } else if (error.code == 2) {
            $("#eatnow-failed-code2").show()
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("You seem to be having trouble acquiring a GPS location. Make sure you take your device to an area without obstructions, and 4 satellites are in view!")
            }
        } else if (error.code == 3) {
            $("#eatnow-failed-code3").show()
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("You seem to be having trouble acquiring a GPS location. Have you considered upgrading your GPS unit that's from 1999 since it took so damn long?")
            }
        } else {
            $("#eatnow-failed-generic").show()
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("You seem to be having trouble acquiring a GPS location. I don't have any tips for you in true Clippy style.")
            }
        }
        eventctrl.emit(eventctrl.eatNowFailed)
    }

    haversineCalc(lat1,lon1,lat2,lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
        var dLon = this.deg2rad(lon2-lon1); 
        var a = 
          Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
          Math.sin(dLon/2) * Math.sin(dLon/2)
          ; 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c; // Distance in km
        return d / 1.60934;
      }
      
    deg2rad(deg) {
        return deg * (Math.PI/180)
    }
}// eslint-disable-next-line no-unused-vars
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16).replace("-", "");
    });
}class Appearance {
    constructor(stylesheet, feedbackspan, bodyhtml) {
        this.stylesheet = stylesheet
        this.feedbackspan = feedbackspan
        this.bodyhtml = bodyhtml
        this.appearance = localStorage.getItem("appearance")
        this.pcs = window.matchMedia('(prefers-color-scheme: dark)')
        window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', this.auto_change.bind(this));
        if (this.appearance == "automatic") {
            $(this.feedbackspan).html("Automatic")
        } else if (this.appearance == "light") {
            $(this.feedbackspan).html("Light")
        } else if (this.appearance == "dark") {
            $(this.feedbackspan).html("Dark")
        }
        consolelog("Appearance", "A new apperance object is being initialized")
        document.addEventListener(eventctrl.appearanceChanged.type, this.manual_change.bind(this))
    }

    init(onload) {
        consolelog("Appearance/init", "Called with onload: " + onload)
        if (this.appearance == "automatic") {
            this.auto_change()
        } else if (this.appearance == "light") {
            consolelog("Appearance/init", "Light mode is now active.")
            $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', true);
        } else if (this.appearance == "dark") {
            consolelog("Appearance/init", "Dark mode is now active.")
            $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', false);
        }

        if (onload) {
            consolelog("Appearance/init", "removing default bg color (from onload)")
            $(this.bodyhtml).css("background-color", "")
        }
    }

    auto_change() {
        if (this.appearance == "automatic") {
            if ((window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
                consolelog("Appearance/auto_change", "Going into dark mode from auto change")
                $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', false);
            } else {
                consolelog("Appearance/auto_change", "Going into light mode from auto change")
                $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', true);
            }
        }
    }

    manual_change() {
        if (this.appearance == "light") {
            consolelog("Appearance/manual_change", "Going from Light -> Dark")
            this.appearance = "dark"
            localStorage.setItem("appearance", "dark")
            $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', false);
            $(this.feedbackspan).html("Dark")
        } else if (this.appearance == "dark") {
            consolelog("Appearance/manual_change", "Going from Dark -> Automatic")
            this.appearance = "automatic"
            localStorage.setItem("appearance", "automatic")
            $(this.feedbackspan).html("Automatic")
            this.auto_change()
        } else if (this.appearance == "automatic") {
            consolelog("Appearance/manual_change", "Going from Automatic -> Light")
            this.appearance = "light"
            localStorage.setItem("appearance", "light")
            $('link[rel="stylesheet"][href="' + this.stylesheet + '"]').prop('disabled', true);
            $(this.feedbackspan).html("Light")
        }
    }
}