/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

class ReoccurUI {
    constructor (label, step, content, timesinput, downame) {
        this.label = label
        this.step = step
        this.content = content
        this.timesinput = timesinput
        this.downame = downame
        consolelog("ReoccurUI", "New object created")
    }

    updatelabel(string) {
        if (string == undefined) {
            var labeltext = ""
            labeltext = $(this.timesinput).val() == "1" ? "1 reservation" : $(this.timesinput).val() + " reservations"
            if ($(this.timesinput).val() != "1") {
                var dayspicked = 0
                $('input[name=' + this.downame + ']').each(function () {
                    this.checked ? dayspicked = dayspicked + 1 : false
                });
                if (dayspicked != 7) {
                    labeltext = labeltext + ", reoccurring on "
                    $('input[name=' + this.downame + ']').each(function () {
                        var sThisVal = (this.checked ? $(this).val() : "");
                        if (dow[sThisVal] != undefined) {
                            labeltext = labeltext + dow[sThisVal].substring(0, 3) + ", "
                        }
                    });
                    labeltext = labeltext.substring(0, labeltext.length - 2)
                } else {
                    labeltext = labeltext + ", reoccurring every day"
                }
            }
            $(this.label).attr("data-step-label", labeltext)
            consolelog("ReoccurUI/updatelabel", "Label being updated to " + labeltext)
        } else {
            $(this.label).attr("data-step-label", string)
            consolelog("ReoccurUI/updatelabel", "Label is now " + string)
        }
    }

    disable(state) {
        consolelog("ReoccurUI/disable", "Disabling with state: " + state)
        if (state) {
            $(this.step).addClass("disabled")
        } else {
            $(this.step).removeClass("disabled")
        }
    }
}