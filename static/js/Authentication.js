/**
 * Handles authentication with the Dine On Campus servers on first run, along with
 * checking for the User ID.
 */

/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

// eslint-disable-next-line no-unused-vars
class Authentication {
    /**
     * Constructs a new Authentication class.
     * @param {} key The key in the local storage to retrieve the User ID from.
     * @param {*} ee The email element to grab values from when logging in.
     * @param {*} pe The password element to grab values from when logging in.
     * @param {*} se The submit button element.
     */
    constructor(key, ee, pe, se, ue, seue) {
        this.key = key
        this.ee = ee
        this.pe = pe
        this.se = se
        this.ue = ue
        this.seue = seue
        this.se_origcontent = $(this.se).html()
        this.seue_origcontent = $(this.seue).html()
        this.userid = ""
        this.loadingcontent = '<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>'
        $(se).attr("onclick", "eventctrl.emit(eventctrl.loginAttempt)")
        $(seue).attr("onclick", "eventctrl.emit(eventctrl.loginUserIDAttempt)")
        document.addEventListener(eventctrl.loginAttempt.type, this.login.bind(this))
        document.addEventListener(eventctrl.logOut.type, this.logout.bind(this))
        document.addEventListener(eventctrl.loginUserIDAttempt.type, this.login_userid.bind(this))
        consolelog("Authentication", "A new Authentication object has been constructed.")
    }

    /**
     * Does a local login, i.e., logins in via known local storage key.
     */
    local_login() {
        consolelog("Authentication/local_login", "Local Login is being attempted")
        if (localStorage.getItem(this.key) != null) {
            this.userid = localStorage.getItem(this.key);
            consolelog("Authentication/local_login", "Local login success, UID " + this.userid)
            document.dispatchEvent(eventctrl.initLoginSuccess)
            return
        } else {
            consolelog("Authentication/local_login", "Local login failure.")
            document.dispatchEvent(eventctrl.initLoginFailure)
            return
        }
    }

    login_userid() {
        consolelog("Authentication/login_userid", "UID login attempting")
        var fieldval = $(this.ue).val()
        var input_userid = ""
        if (fieldval.includes("%22")) {
            consolelog("Authentication/login_userid", "Parsing UID via Cookie")
            // Parsing the raw cookie value
            var fieldarray = fieldval.split("%22")
            // With current cookie formatting it will be the 3rd element
            input_userid = fieldarray[3]
        } else {
            // Replace potential quotes
            input_userid = fieldval
            input_userid.replace("'", "")
            input_userid.replace('"', "")
        }

        if (input_userid.length != 24) {
            consolelog("Authentication/login_userid", "UID length invalid at length " + input_userid.length)
            this.login_validation_userid(false)
            return
        }

        var url = "https://api.dineoncampus.com/v1/customer/info?customer_id=" + input_userid
        var xhr = new XHRwrap(url, "GET", true, 30000, null, this.login_userid_back.bind(this))
        xhr.request()
        this.login_validation_userid(null)
        $(this.seue).prop("disabled", true)
        $(this.seue).html(this.loadingcontent + "" + this.seue_origcontent)
    }

    login_userid_back(response) {
        try {
            var data = response.responseText;
            var obj = JSON.parse(data);
        } catch {
            consolelog("Authentication/login_userid_back", "Verification failed, failed at parsing JSON")
            this.login_validation_userid(false)
        }

        $(this.seue).prop("disabled", false)
        $(this.seue).html(this.seue_origcontent)

        try {
            var status = obj['status']
            if (status == "success") {
                consolelog("Authentication/login_userid_back", "Verification successful!")
                localStorage.setItem("userid", obj['customer']['id'])
                this.userid = obj['customer']['id']
                this.login_validation_userid(true)
                setTimeout(function () {
                    document.dispatchEvent(eventctrl.loginUserIDSuccess)
                }, 500)
                setTimeout(function () {
                    document.dispatchEvent(eventctrl.initLoginSuccess)
                }, 1000)
                setTimeout(function () {
                    $(this.ue).val("")
                    $(this.ee).val("")
                    $(this.pe).val("")
                }.bind(this), 2000)
            } else {
                consolelog("Authentication/login_userid_back", "Verification failure, didn't see success as status")
                this.login_validation_userid(false)
            }
        } catch {
            consolelog("Authentication/login_userid_back", "Verification failed, failed post-JSON parse")
            this.login_validation_userid(false)
        }
    }

    login_validation_userid(useridvalid) {
        consolelog("Authentication/login_validation_userid", "Updating UID validation, UID valid is " + useridvalid)
        $(this.ue).removeClass("is-invalid").removeClass("is-valid")
        if (useridvalid == true) {
            $(this.ue).addClass("is-valid")
            setTimeout(function() {
                $(this.ue).removeClass("is-valid")
            }.bind(this), 2000)
        } else if (useridvalid == false) {
            $(this.ue).addClass("is-invalid")
        }
    }

    /**
     * Updates validation for login.
     * @param {*} emailvalid 
     * @param {*} passvalid 
     * @param {*} reset 
     */
    login_validation(emailvalid, passvalid) {
        consolelog("Authentication/login_validation", "Updating login validation, emailvalid is " + emailvalid + ", passvalid is " + passvalid)
        $(this.ee).removeClass("is-invalid").removeClass("is-valid")
        $(this.pe).removeClass("is-invalid").removeClass("is-valid")
        // Explicit checking because emailvalid/passvalid can be null (just reset)
        if (emailvalid == true) {
            $(this.ee).addClass("is-valid")
            setTimeout(function() {
                $(this.ee).removeClass("is-valid")
            }.bind(this), 2000)
        } else if (emailvalid == false) {
            $(this.ee).addClass("is-invalid")
        }

        if (passvalid == true) {
            $(this.pe).addClass("is-valid")
            setTimeout(function () {
                $(this.pe).removeClass("is-valid")
            }.bind(this), 2000)
        } else if (passvalid == false) {
            $(this.pe).addClass("is-invalid")
        }
    }

    /**
     * Does a Dine On Campus server login.
     */
    login() {
        consolelog("Authentication/login", "Beginning login process")
        // eslint-disable-next-line no-undef
        var token = uuidv4();
        var url = "https://api.dineoncampus.com/v1/customer/authenticate.json?email=" + $(this.ee).val() + "&password=" + $(this.pe).val() + "&token=" + token 
        var xhr = new XHRwrap(url, "GET", true, 30000, null, this.loginback.bind(this))
        xhr.request()
        this.login_validation(null, null)
        $(this.se).prop("disabled", true)
        $(this.se).html(this.loadingcontent + "" + this.se_origcontent)
    }

    logout() {
        consolelog("Authentication/logout", "Logging out!")
        localStorage.removeItem("userid")
        localStorage.removeItem("appearance")
        localStorage.removeItem("dakamode")
        localStorage.removeItem("clippymode")
        location.reload()
    }

    /**
     * Callback function for XHRwrap.
     * @param {} request request object from XHRResposeObject
     */
    loginback(request) {
        try {
            var data = request.responseText;
            var obj = JSON.parse(data)
        } catch {
            this.login_validation(false, false)
            consolelog("Authentication/loginback", "Failed to login in, failed during JSON parse")
        }

        $(this.se).prop("disabled", false)
        $(this.se).html(this.se_origcontent)

        try {
            var userid = obj['customer']['id']
            localStorage.setItem("userid", userid)
            consolelog("Authentication/loginback", "Login successful!")
            this.login_validation(true, true)
            setTimeout(function() {
                document.dispatchEvent(eventctrl.loginSuccess)
            }, 500)
            setTimeout(function() {
                document.dispatchEvent(eventctrl.initLoginSuccess)
            }, 1000)
            setTimeout(function() {
                $(this.ee).val("")
                $(this.pe).val("")
                $(this.ue).val("")
            }.bind(this), 2000)
        } catch {
            this.login_validation(false, false)
            consolelog("Authentication/loginback", "Login failed post-JSON parse")
        }
    }
}