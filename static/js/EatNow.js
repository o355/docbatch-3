/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

class EatNow {
    constructor (s1ctrl, frmctrl, locations) {
        this.s1ctrl = s1ctrl
        this.frmctrl = frmctrl
        this.locations = locations
        consolelog("EatNow", "A new Eat Now object has been created.")
    }

    eatnow() {
        consolelog("EatNow/eatnow", "Eat Now called, waiting 100ms for transition to finish")
        eventctrl.emit(eventctrl.eatNowStart)
        setTimeout(function () {
            this.getlocation()
        }.bind(this), 100)
    }

    getlocation() {
        var options = {
            enableHighAccuracy: true,
            maximumAge: 0,
            timeout: 30000
        }
        navigator.geolocation.getCurrentPosition(this.getlocation_success.bind(this), this.getlocation_fail.bind(this), options)
        consolelog("EatNow/getlocation", "geolocation request went through")
    }

    getlocation_success(position) {
        var latitude = position.coords.latitude
        var longitude = position.coords.longitude
        consolelog("EatNow/getlocation_success", "Geolocation good, user location is " + latitude + " " + longitude)
        var locarray = this.locations.getlocations()
        var closestid;
        var closestid_distance = 99999;
        var location_closestid;
        var location_distance = 99999;
        console.group("%c[EatNow/getlocation_success]%c Parsing locations", "color: purple, font-weight: bold", "")
        Object.keys(locarray).forEach(function (k) {
            consolelog("EatNow/getlocation_success", "Parsing for " + k)
            if (k == "5f344f0b0101560dbd3cb05b") {
                consolelog("EatNow/getlocation_success", "Is on blacklist, continuing...")
                return
            }

            if (locarray[k]['reservations'] == false) {
                consolelog("EatNow/getlocation_success", "Does not accept reservations, continuing...")
                return
            }
            var opstatus = this.locations.getoperationstatus(k)
            var distance = this.haversineCalc(locarray[k]['lat'], locarray[k]['lng'], latitude, longitude)
            if (opstatus['status'] == "closed") {
                consolelog("EatNow/getlocation_success", "Location is closed!")
                if (distance < location_distance) {
                    location_closestid = k
                    location_distance = distance
                }
                return
            }

            if (distance <= 1.0 && distance < closestid_distance) {
                consolelog("EatNow/getlocation_success", "Closest location found! Distance is " + distance + " (compared to dist of " + closestid_distance + ")")
                closestid = k
                closestid_distance = distance
            } else {
                consolelog("EatNow/getlocation_success", "Location is further than 1 mile away (distance = " + distance + ")")
                if (distance < location_distance) {
                    consolelog("EatNow/getlocation_success", "Closest location found! Distance is " + distance + " (compared to dist of " + closestid_distance + ")")
                    location_closestid = k
                    location_distance = distance
                }
            }
        }.bind(this))
        console.groupEnd()

        if (closestid != undefined) {
            consolelog("EatNow/getlocation_success", "Found a closest location with id " + closestid)
            eventctrl.emit(eventctrl.eatNowSuccess)
            $("input[name='" + this.s1ctrl.formname + "'][value='" + closestid + "']").prop("checked", true)
            this.frmctrl.reservefornow(true)
            $("#eatnow-nothing-expl-normal").show()
            $("#eatnow-nothing-expl-toofar").hide()
        } else {
            consolelog("EatNow/getlocation_success", "Failed to find closest location...")
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("I'm sorry I couldn't find any locations for you to eat now at.")
            }
            $("#eatnow-nothing-expl-normal").show()
            $("#eatnow-nothing-expl-toofar").hide()
            if (location_distance > 1.0) {
                consolelog("EatNow/getlocation_success", "...Because everything was too far away (nearest = " + location_distance + ")")
                $("#eatnow-nothing-expl-normal").hide()
                $("#eatnow-nothing-expl-toofar").show()
                $("#eatnow-nothing-exp-toofar-1").html(this.locations.getlocationname(location_closestid))
                $("#eatnow-nothing-exp-toofar-2").html(location_distance.toFixed(2) + " miles ")
            }
            eventctrl.emit(eventctrl.eatNowNothing)
        }
    }

    getlocation_fail(error) {
        consolelog("EatNow/getlocation_fail", "Failed to get location with EC " + error.code)
        $("#eatnow-failed-code1").hide()
        $("#eatnow-failed-code2").hide()
        $("#eatnow-failed-code3").hide()
        $("#eatnow-failed-generic").hide()
        if (error.code == 1) {
            $("#eatnow-failed-code1").show()
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("You seem to be having trouble understanding that I need to know your location so I can find where you can eat. Have you tried getting some more brain cells?")
            }
        } else if (error.code == 2) {
            $("#eatnow-failed-code2").show()
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("You seem to be having trouble acquiring a GPS location. Make sure you take your device to an area without obstructions, and 4 satellites are in view!")
            }
        } else if (error.code == 3) {
            $("#eatnow-failed-code3").show()
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("You seem to be having trouble acquiring a GPS location. Have you considered upgrading your GPS unit that's from 1999 since it took so damn long?")
            }
        } else {
            $("#eatnow-failed-generic").show()
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("You seem to be having trouble acquiring a GPS location. I don't have any tips for you in true Clippy style.")
            }
        }
        eventctrl.emit(eventctrl.eatNowFailed)
    }

    haversineCalc(lat1,lon1,lat2,lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
        var dLon = this.deg2rad(lon2-lon1); 
        var a = 
          Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
          Math.sin(dLon/2) * Math.sin(dLon/2)
          ; 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c; // Distance in km
        return d / 1.60934;
      }
      
    deg2rad(deg) {
        return deg * (Math.PI/180)
    }
}