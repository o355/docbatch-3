/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

/**
 * Orchestrates the DOM Events triggered throughout DOCBatch. Mainly to
 * prevent hardcoded event names, and rather references to a single object.
 */
// eslint-disable-next-line no-unused-vars
class Events {
    constructor() {
        this.initLoginSuccess = new Event("logininit-good")
        this.initLoginFailure = new Event("logininit-bad")
        this.loginSuccess = new Event("login-good")
        this.loginFailure = new Event("login-bad")
        this.loginAttempt = new Event("login-attempt")
        this.failedLoad = new Event("failed-load")
        this.successLoad = new Event("success-load")
        this.makeReservations = new Event("make-reservations")
        this.makeReservationsFail = new Event("make-reservations-fail")
        this.makeReservationsSuccess = new Event("make-reservations-success")
        this.deleteReservationsInit = new Event("delete-reservations-init")
        this.deleteReservationsP1Fail = new Event("delete-reservations-p1fail")
        this.deleteReservationsP1Fail2 = new Event("delete-reservations-p1fail2")
        this.deleteReservationsP1None = new Event("delete-reservations-p1none")
        this.deleteReservationsP1None2 = new Event("delete-reservations-p1none2")
        this.deleteReservationsP1Success = new Event("delete-reservations-p1success")
        this.deleteReservationsFail = new Event("delete-reservations-fail")
        this.deleteReservationsFail2 = new Event("delete-reservations-fail2")
        this.deleteReservationsSuccess = new Event("delete-reservations-success")
        this.deleteReservationsSuccess2 = new Event("delete-reservations-success2")
        this.deletePendingReservations = new Event("delete-pending-reservations")
        this.logOut = new Event("logout")
        this.backToStart = new Event("backtostart")
        this.reinitSuccess = new Event("reinit-success")
        this.reinitFailure = new Event("reinit-failure")
        this.loginSwitchUserID = new Event("login-switchuserid")
        this.loginSwitchNormal = new Event("login-switchnormal")
        this.loginUserIDAttempt = new Event("login-useridattempt")
        this.loginUserIDSuccess = new Event("login-useridsuccess")
        this.eatNowStart = new Event("eatnow-start")
        this.eatNowFailed = new Event("eatnow-failed")
        this.eatNowNothing  = new Event("eatnow-nothing")
        this.eatNowSuccess = new Event("eatnow-success")
        this.eatNowFailedReset = new Event("eatnow-failed-reset")
        this.eatnowNothingReset = new Event("eatnow-nothing-reset")
        this.locationSelected = new Event("location-selected")
        this.appearanceChanged = new Event("appearance-changed")
        this.deleteReservationsP1SpamWarn = new Event("delete-reservations-p1-spamwarn")
        this.deleteReservationsP1SpamDeny = new Event("delete-reservations-p1-spamdeny")
        this.deleteReservationsP1SpamConfirm = new Event("delete-reservations-p1-spamconfirm")
        this.aprilFoolsFadeIn = new Event("april-fools-fadein")
        this.aprilFoolsFadeOut = new Event("april-fools-fadeout")
        this.shutdownFade = new Event("shutdown-fadein")
        consolelog("Events", "New Events object has been created.")
    }

    emit(event) {
        consolelog("Events/emit", "Emitting event " + event.type)
        document.dispatchEvent(event)
    }
}

// eslint-disable-next-line no-unused-vars
var eventctrl = new Events()