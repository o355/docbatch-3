/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

/**
 * Handles the Dine On Campus locations. Provides translation between names & IDs, and vice versa.
 */
class Locations {
    /**
     * Constructs a new Locations object.
     * @param {} url The URL of the DOCBatch API to call to get location data.
     * @param {} ec An element (or class name) that can be written to for error code purposes.
     * @param {} msg An element (or class name) that can be written to for error code purposes.
     * @param {} locationsform An element to serve as the locations form, to which locations will be written to.
     * @param {} radiocopy The ID (NOT JQUERY REFERENCE) of the element to copy to in the locations form. Must also be the prefix for subelements (_hours, _location)
     */
    constructor (url, ec, msg, locationsform, radiocopy) {
        this.url = url
        this.ec = ec
        this.msg = msg
        this.locations = {}
        this.locationsarray = []
        this.locationsform = locationsform
        this.radiocopy = radiocopy
        this.opencolor = "greentext"
        this.partialopencolor = "orangetext"
        this.closedcolor = "redtext"
        document.addEventListener(eventctrl.initLoginSuccess.type, this.init.bind(this))
    }

    /**
     * Initializes the locations database from the URL to be called. This is usually done after a confirmed successful login, but
     * could be done before.
     */
    init() {
        var url = this.url
        var xhr = new XHRwrap(url, "GET", true, 30000, null, this.initcallback.bind(this))
        xhr.request()
    }

    /**
     * Gets a location name from an ID
     * @param {*} id An ID
     */
    getlocationname(id) {
        return this.locationsarray[id]
    }

    getlocations() {
        return this.locations
    }

    getlocationsarray() {
        return this.locationsarray
    }

    reservationsrequired(k) {
        return this.locations[k]['reservations']
    }

    getoperationstatus(k) {
        var timestamp = Math.floor(Date.now() / 1000)
        for (var i = 0; i < this.locations[k]['hours'].length; i = i + 2) {
            var starttime = this.locations[k]['hours'][i]
            var endtime = this.locations[k]['hours'][i + 1]

            if (timestamp >= starttime && timestamp >= endtime) {
                continue
            } else if (timestamp >= starttime && timestamp <= endtime) {
                var seconds_open = endtime - timestamp
                return {"status": "open", "secondsleft": seconds_open}
            } else if (timestamp <= starttime && timestamp <= endtime) {
                var seconds_left = starttime - timestamp
                return {"status": "closed", "secondsleft": seconds_left};
            }
        }
        return {"status": "closed", "secondsleft": -1}
    }

    /**
     * Data from the callback to init Locations.
     * @param {} request XHRHttpRequest request thing.
     */
    initcallback(request) {
        try {
            var data = request.responseText;
            var obj = JSON.parse(data)
        } catch {
            $(this.ec).html("320")
            $(this.msg).html("Failed to load location data.")
            eventctrl.emit(eventctrl.failedLoad)
            return
        }

        var outerHTML = $("#" + this.radiocopy).prop("outerHTML");

        $("#" + this.radiocopy).attr("id", "preventduplicity");
        $("#" + this.radiocopy + "_label").attr("id", "preventduplicity_label")
        $("#" + this.radiocopy + "_input").attr("id", "preventduplicity_input")
        $("#" + this.radiocopy + "_location").attr("id", "preventduplicity_location")
        $("#" + this.radiocopy + "_hours").attr("id", "preventduplicity_hours")

        $(this.locationsform).empty()
        Object.keys(obj['data']).forEach(function (k) {
            if (obj['data'][k]['name'].search("Morgan") != -1 && (localStorage.getItem("dakamode") == "true")) {
                this.locationsarray[k] = "DAKA"
            } else {
                this.locationsarray[k] = obj['data'][k]['name']
            }
            $(this.locationsform).prepend(outerHTML)
            $("#" + this.radiocopy).attr("id", k)
            $("#" + this.radiocopy + "_label").attr("id", k + "_label")
            $("#" + this.radiocopy + "_input").attr("id", k + "_input")
            $("#" + this.radiocopy + "_location").attr("id", k + "_name")
            $("#" + this.radiocopy + "_hours").attr("id", k + "_hours")
            $("#" + k + "_name").html(this.locationsarray[k])
            $("#" + k + "_label").attr("for", k + "_input")
            $("#" + k + "_input").attr("name", "locations-form")
            $("#" + k + "_input").attr("value", k)
            this.locations[k] = {}
            this.locations[k]['hrspan'] = $("#" + k + "_hours")
            this.locations[k]['hours'] = obj['data'][k]['thisweekhours']['L'].concat(obj['data'][k]['nextweekhours']['L']) 
            this.locations[k]['lat'] = obj['data'][k]['lat']
            this.locations[k]['lng'] = obj['data'][k]['lng']
            this.locations[k]['reservations'] = obj['data'][k]['reservations']
            // Need to undisable in OSS.
            //if (obj['data'][k]['reservations'] == false) {
            //    $("#" + k + "_input").attr("disabled", true)
            //}
        }.bind(this))

        $("#preventduplicity").attr("id", this.radiocopy)
        $("#preventduplicity_input").attr("id", this.radiocopy + "_input")
        $("#preventduplicity_location").attr("id", this.radiocopy + "_location")
        $("#preventduplicity_hours").attr("id", this.radiocopy + "_hours")
        window.onfocus = this.updateTimes.bind(this)
        setInterval(this.updateTimes.bind(this), 10000)
        this.updateTimes()
        setTimeout(function() {
            eventctrl.emit(eventctrl.successLoad)
        }, 250)
    }

    /**
     * Update the times.
     */
    updateTimes() {
        var timestamp = Math.floor(Date.now() / 1000)
        var curobj = new Date()
        Object.keys(this.locations).forEach(function (k) {
            for (var i = 0; i < this.locations[k]['hours'].length; i = i + 2) {
                var starttime = this.locations[k]['hours'][i]
                var endtime = this.locations[k]['hours'][i + 1]
                var starttime_dateobj = new Date(starttime * 1000)
                var endtime_dateobj = new Date(endtime * 1000)

                if (timestamp >= starttime && timestamp >= endtime) {
                    continue
                } else if (timestamp >= starttime && timestamp <= endtime) {
                    var seconds_open = endtime - timestamp
                    // eslint-disable-next-line no-undef
                    var closestr = hm_tostring(endtime_dateobj.getHours(), endtime_dateobj.getMinutes())
                    if (seconds_open > 3600) {
                        $(this.locations[k]['hrspan']).html("Open! Closes at " + closestr + ".").removeClass().addClass(this.opencolor)
                    } else {
                        if (seconds_open <= 60) {
                            $(this.locations[k]['hrspan']).html("Open, but closes in " + Math.ceil(seconds_open / 60) + " minute.").removeClass()
                        } else {
                            $(this.locations[k]['hrspan']).html("Open, but closes in " + Math.ceil(seconds_open / 60) + " minutes.").removeClass()
                        }
                        if (seconds_open > 300) {
                            $(this.locations[k]['hrspan']).addClass(this.partialopencolor)
                        } else {
                            $(this.locations[k]['hrspan']).addClass(this.closedcolor)
                        }
                    }
                    return
                } else if (timestamp <= starttime && timestamp <= endtime) {
                    // eslint-disable-next-line no-undef
                    var openstr = hm_tostring(starttime_dateobj.getHours(), starttime_dateobj.getMinutes())
                    // eslint-disable-next-line no-undef
                    var openday = dow[starttime_dateobj.getDay()]
                    var seconds_closed = starttime - timestamp
                    if (starttime - timestamp < 3600) {
                        if (seconds_closed <= 60) {
                            $(this.locations[k]['hrspan']).html("Closed, but opens in " + Math.ceil(seconds_closed / 60) + " minute.").removeClass().addClass(this.closedcolor)
                        } else {
                            $(this.locations[k]['hrspan']).html("Closed, but opens in " + Math.ceil(seconds_closed / 60) + " minutes.").removeClass().addClass(this.closedcolor)
                        }
                    } else if (starttime_dateobj.getDay() == curobj.getDay()) {
                        $(this.locations[k]['hrspan']).html("Closed. Opens at " + openstr + ".").removeClass().addClass(this.closedcolor)
                    } else if (starttime - timestamp < 86400) {
                        $(this.locations[k]['hrspan']).html("Closed. Opens tomorrow at " + openstr + ".").removeClass().addClass(this.closedcolor)
                    } else if (starttime - timestamp < 604800) {
                        $(this.locations[k]['hrspan']).html("Closed. Opens " + openday + " at " + openstr + ".").removeClass().addClass(this.closedcolor)
                    } else {
                        $(this.locations[k]['hrspan']).html("Closed. Opens next " + openday + " at " + openstr + ".").removeClass().addClass(this.closedcolor)
                    }
                    return;
                }
            }
            $(this.locations[k]['hrspan']).html("Closed.").removeClass().addClass(this.closedcolor)
        }.bind(this))
    }

}