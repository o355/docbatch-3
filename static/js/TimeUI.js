/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

class TimeUI {
    constructor (label, step, content, input, invalidfeedback) {
        this.label = label
        this.step = step
        this.content = content
        this.input = input
        this.invalidfeedback = invalidfeedback
        $(this.input).change(this.updatetime.bind(this))
        consolelog("TimeUI", "Created new Time UI object.")
    }

    autotime(offset) {
        var timestamp = (new Date().getTime() / 1000) + (offset * 60)
        var adjusted = Math.round(timestamp / 1800)
        var finalts = adjusted * 1800
        var momentobj = moment.unix(finalts)
        $(this.input).val(momentobj.format("h:mm A"))
        consolelog("TimeUI/autotime", "Auto time created with offset " + offset + " and result " + momentobj.format("h:mm A"))
    }

    changetime(offset) {
        if ($(this.input).val() == "") {
            this.autotime(offset)
            return
        }
        var momentobj = moment($(this.input).val(), "h:mm A")
        var currentdate = momentobj.date()

        momentobj.add(offset, "minutes")
        if (momentobj.date() == currentdate) {
            $(this.input).val(momentobj.format("h:mm A"))
            consolelog("TimeUI/changetime", "Change time success with offset " + offset + " and result " + momentobj.format("h:mm A"))
        }
    }

    updatetime() {
        var text = $(this.input).val()
        // Matches if the length of the text is 7 (always will be), has an AM/PM, and has zero spaces.
        // I would prefer not having to do this, but the included stuff doesn't work :/
        if (text.length != 7 || !(text.includes("AM") || text.includes("PM")) || text.includes(" ")) {
            consolelog("TimeUI/updatetime", "Time label formatting is not being updated. Conditions not met.")
            return
        }

        if (text.charAt(0) == "0") {
            text = text.substring(1)
        }

        // Ternary operators mean this is super clean.
        consolelog("TimeUI/updatetime", "Time label formatting is being updated.")
        $(this.input).val(text.split("A").length == 2 ? text = text.split("A")[0] + " AM" : text = text.split("P")[0] + " PM")
    }

    validate(onclick) {
        if (onclick) {
            if (!$(this.content).is(":visible")) {
                consolelog("TimeUI/validate", "Validation on-click failing, content is not visible.")
                return false
            }
        }

        if (moment($(this.input).val(), "h:mm A").isValid()) {
            $(this.step).removeClass("invalid")
            this.updatelabel()
            $(this.input).removeClass("invalid").addClass("valid")
            $(this.invalidfeedback).hide()
            consolelog("TimeUI/validate", "Input validation success!")
            return true
        } else {
            $(this.invalidfeedback).html("Please enter a valid time.")
            $(this.step).addClass("wrong")
            this.updatelabel("")
            $(this.input).addClass("invalid")
            $(this.invalidfeedback).show()
            var tempval = $(this.input).val()
            // This is the magic code that prevents going on to Step 3 onclick.
            // Basically clears the value so that the form thinks you need to enter a value. But
            // we put back the user's input 1ms later.

            // CSS magic by turning down the opacity of the default label feedback to 0 also makes it so that we 100%
            // use our custom validation.
            if (onclick) {
                $(this.input).val("")
            }
            setTimeout(function (val) {
                $(this.input).val(val)
            }.bind(this, tempval), 1)
            if (clippymode) {
                clippyagent.stop()
                if (Math.random() * 10 <= 5) {
                    clippyagent.speak("It seems like you're having trouble entering a time. Make sure you include the hours and the minutes!")
                } else {
                    clippyagent.speak("It seems like you're having trouble entering a time. Try something like 4:20 PM.")
                } 
            }
            consolelog("TimeUI/validate", "Input validation failure.")
            return false
        }
    }

    updatelabel(string) {
        if (string == undefined) {
            $(this.label).attr("data-step-label", $(this.input).val())
            consolelog("TimeUI/updatelabel", "Label is being updated to " + $(this.input).val())
        } else {
            $(this.label).attr("data-step-label", string)
            consolelog("TimeUI/updatelabel", "Label updated to " + string)
        }
    }

    disable(state) {
        consolelog("TimeUI/disable", "Disabling with state: " + state)
        if (state) {
            $(this.step).addClass("disabled")
        } else {
            $(this.step).removeClass("disabled")
        }
    }
}