/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

class DateUI {
    constructor(label, step, content, input, invalidfeedback) {
        this.label = label
        this.step = step
        this.content = content
        this.input = input
        this.invalidfeedback = invalidfeedback
        consolelog("DateUI", "A new Date UI object is being created.")
    }

    autodate(offset) {
        var timestamp = (new Date().getTime() / 1000) + offset
        var adjusted = Math.round(timestamp / 1800)
        var finalts = adjusted * 1800
        var momentobj = moment.unix(finalts)
        consolelog("DateUI/autodate", "Autodate activated with offset " + offset + ", end result " + momentobj.format("MM/DD/YYYY"))
        $(this.input).val(momentobj.format("MM/DD/YYYY"))
    }

    validate(onclick) {
        consolelog("DateUI/validate", "Validating...")
        if (onclick) {
            if (!$(this.content).is(":visible")) {
                consolelog("DateUI/validate", "Validate onclick failed, content not visible.")
                return false
            }
        }

        if (moment($(this.input).val(), "MM/DD/YYYY").isValid()) {
            $(this.step).removeClass("invalid")
            this.updatelabel()
            $(this.input).removeClass("invalid").addClass("valid")
            $(this.invalidfeedback).hide()
            consolelog("DateUI/validate", "Non-onclick validation success")
            return true
        } else {
            if ($(this.input).val().includes("poopoo") && $(this.input).val().includes("peepee")) {
                $(this.invalidfeedback).html("haha hehe")
            } else if ($(this.input).val().includes(";)")) {
                $(this.invalidfeedback).html("*bonk* Go to horny jail! Now!")
            } else if ($(this.input).val().includes("fuck you")) {
                $(this.invalidfeedback).html("Oh fuck off will you")
            } else {
                $(this.invalidfeedback).html("Please enter a valid date.")
            }
            $(this.step).addClass("wrong")
            this.updatelabel("")
            $(this.input).addClass("invalid")
            $(this.invalidfeedback).show()
            var tempval = $(this.input).val()
            // This is the magic code that prevents going on to Step 3 onclick.
            // Basically clears the value so that the form thinks you need to enter a value. But
            // we put back the user's input 1ms later.

            // CSS magic by turning down the opacity of the default label feedback to 0 also makes it so that we 100%
            // use our custom validation.
            if (onclick) {
                $(this.input).val("")
            }
            setTimeout(function (val) {
                $(this.input).val(val)
            }.bind(this, tempval), 1)
            if (clippymode) {
                clippyagent.stop()
                clippyagent.speak("It seems like you're having trouble selecting a date. Try picking today!")
            }
            consolelog("DateUI/validate", "Non-onclick validation failure")
            return false
        }
    }

    updatelabel(string) {
        if (string == undefined) {
            $(this.label).attr("data-step-label", $(this.input).val())
            consolelog("DateUI/updatelabel", "Label is being updated to " + $(this.input).val())
        } else {
            $(this.label).attr("data-step-label", string)
            consolelog("DateUI/updatelabel", "Label is being updated to " + string)
        }
    }

    disable(state) {
        consolelog("DateUI/disable", "DateUI disable state: " + state)
        if (state) {
            $(this.step).addClass("disabled")
        } else {
            $(this.step).removeClass("disabled")
        }
    }
}