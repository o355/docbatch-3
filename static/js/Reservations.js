/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

/**
 * Class to hold all the reservations, which is just a bunch of Reservation.
 */
class Reservations {
    // To be constructed
    constructor(s1ctrl, s2ctrl, s3ctrl, s4ctrl, s5ctrl, s6ctrl, auth, confirmbutton) {
        this.reservations = []
        this.s1ctrl = s1ctrl
        this.s2ctrl = s2ctrl
        this.s3ctrl = s3ctrl
        this.s4ctrl = s4ctrl
        this.s5ctrl = s5ctrl
        this.s6ctrl = s6ctrl
        this.auth = auth
        this.confirmbutton = confirmbutton
        $(this.confirmbutton).hide()
        document.addEventListener(eventctrl.makeReservations.type, this.makereservations.bind(this, 0))
        document.addEventListener(eventctrl.deletePendingReservations.type, this.deleteallreservations.bind(this))
        consolelog("Reservations", "Created new reservations object.")
    }
    
    deletereservation(index) {
        consolelog("Reservations/deletereservation", "Deleting reservation at index " + index)
        this.reservations.splice(index, 1)
        this.s5ctrl.updatetable(this)
        this.reservations.length == 1 ? this.s5ctrl.updatelabel("1 reservation") : this.s5ctrl.updatelabel(this.reservations.length + " reservations")
        if (this.reservations.length == 0) {
            consolelog("Reservations/deletereservation", "0 reservations exist, going back to start of form")
            $(this.confirmbutton).hide()
            if ($(this.s5ctrl.content).is(":visible")) {
                eventctrl.emit(eventctrl.backToStart)
            }
        }
    }

    deleteallreservations() {
        consolelog("Reservations/deleteallreservations", "Deleting all reservations!")
        var totallength = this.reservations.length
        for (var i = 0; i < totallength; i++) {
            this.deletereservation(0)
        }
    }
    // Method that takes in a unix timestamp and shoves out a reservation into the array. No complicated stuff needed.
    createreservation(timestamp, location, collapse) {
        $(this.confirmbutton).show()
        var momentobj = moment.unix(timestamp)
        var reserv = new Reservation(momentobj.format("YYYY-MM-DD"), momentobj.hour(), momentobj.minute(), location, momentobj)
        consolelog("Reservations/createreservation", "Created new reservation: from timestamp " + timestamp + " location " + location)
        this.reservations.push(reserv)
        this.reservations.length == 1 ? this.s5ctrl.updatelabel("1 reservation") : this.s5ctrl.updatelabel(this.reservations.length + " reservations")
        this.s5ctrl.updatetable(this, collapse)
    }
    
    createrecursivereservation() {
        consolelog("Reservations/createrecursivereservation", "Now creating recursive reservations.")
        this.createsinglereservation(false)
        var basemoment = moment($(this.s2ctrl.input).val() + " " + $(this.s3ctrl.input).val(), "M/D/YYYY h:mm A")
        var basets = basemoment.unix()
        var acceptabledows = []
        $('input[name=' + this.s4ctrl.downame + ']').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal != undefined) {
                acceptabledows.push(sThisVal)
            }
        });
        for (var i = 0; i < $(this.s4ctrl.timesinput).val() - 1;) {
            basets = basets + 86400
            var modifiedmoment = moment.unix(basets)
            if (acceptabledows.includes(modifiedmoment.day().toString())) {
                this.createreservation(basets, $("input[name=" + this.s1ctrl.formname + "]:checked").val(), true)
                i++
            }
        }
        if (this.reservations.length > 5) {
            setTimeout(function () {
                this.s5ctrl.collapsechange(false)
            }.bind(this), 20)
        }
    }

    makereservations(index) {
        if (index == undefined) {
            index = 0
        }
        this.s6ctrl.updatemadelabel(index)
        this.s6ctrl.updatetotallabel(this.reservations.length)
        var reservobj = this.reservations[index]
        consolelog("Reservations/makereservations", "Making reservation at index " + index)
        var url = "https://api.dineoncampus.com/v1/reservations"
        // The local user ID is being a bit wonky after login, so just get it every time.
        var data = {
            "customer_id": localStorage.getItem("userid"),
            "date": reservobj.date,
            "friends": 0,
            "hour": reservobj.hour,
            "location_id": reservobj.location,
            "minute": reservobj.minute
        }
        var xhr = new XHRwrap(url, "POST", true, 30000, data, this.makereservations_callback.bind(this))
        xhr.request()
    }

    makereservations_callback(request) {
        var data = request.responseText
        try { 
            var obj = JSON.parse(data)
            var status = obj['status']
        } catch (error) {
            consolelog("Reservations/makereservations_callback", "Failed to make reservation (JSON parse). Failing whole process.")
            if (clippymode) {
                clippyagent.stop()
                clippyagent.play("GetAttention")
                clippyagent.speak("I'm sorry I couldn't make your dining reservations. Have you tried redialing into AOL?")
            }
            eventctrl.emit(eventctrl.makeReservationsFail)
            return
        }

        if (obj['status'] != "success") {
            consolelog("Reservations/makereservations_callback", "Status for reservation creation is NOT success. Failing whole process.")
            eventctrl.emit(eventctrl.makeReservationsFail)
        } else {
            var curindex = $(this.s6ctrl.madelabel).html()
            var total = $(this.s6ctrl.totallabel).html()
            if (parseInt(curindex) + 1 == parseInt(total)) {
                this.s6ctrl.updatemadelabel(parseInt(curindex) + 1)
                setTimeout(function () {
                    eventctrl.emit(eventctrl.makeReservationsSuccess)
                    if (clippymode) {
                        clippyagent.stop()
                        clippyagent.play("Congratulate")
                        clippyagent.speak("I'm so glad I could help make your dining reservations. Enjoy your meal!")
                        clearInterval(clippyinterval)
                    }
                }.bind(this), 500)
            } else {
                this.makereservations(parseInt(curindex) + 1)
            }
        }
        
    }

    createsinglereservation(collapsedeny) {
        consolelog("Reservations/createsinglereservation", "Creating single reservation with collapsedeny: " + collapsedeny)
        var momentobj = moment($(this.s2ctrl.input).val() + " " + $(this.s3ctrl.input).val(), "M/D/YYYY h:mm A")
        if (collapsedeny) {
            this.createreservation(momentobj.unix(), $("input[name=" + this.s1ctrl.formname + "]:checked").val(), false)
        } else {
            this.createreservation(momentobj.unix(), $("input[name=" + this.s1ctrl.formname + "]:checked").val(), true)
        }
    }
}