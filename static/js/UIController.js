/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

// eslint-disable-next-line no-unused-vars
class BasicUIController {
    constructor(loadingelem, stepelem) {
        this.loadingelem = loadingelem
        this.stepelem = stepelem
    }

    loadDone() {
        $(this.loadingelem).fadeOut(500);
        $(this.stepelem).fadeIn(500)
    }
}

/**
 * Controls the UI of DOCBatch, including transitions between steps, etc.
 */
// eslint-disable-next-line no-unused-vars
class UIController {
    /**
     * Constructs a new UIController.
     * @param {*} le The loading element to control.
     * @param {*} se The steps element to control.
     * @param {*} lge The login element to control.
     * @param {*} sue The submission element to control.
     * @param {*} fle The failed loading element to control.
     * @param {} ec A variable that can be written to by other classes for when errors occur as the error code.
     * @param {} msg A variable that can be written to by other classes for when errors occur as the message for the error.
     */
    constructor(le, se, lge, sue, fle, suesucc, suefail, dap1, dap1fail, dap1none, da, dafail, dasuccess, ulge, eatl, eatf, eatn, dap1spam, af, shutdown) {
        this.le = le;
        this.se = se;
        this.lge = lge;
        this.sue = sue;
        this.doc = document;
        this.fle = fle;
        this.suesucc = suesucc
        this.suefail = suefail
        this.dap1 = dap1
        this.dap1fail = dap1fail
        this.dap1none = dap1none
        this.da = da
        this.dafail = dafail
        this.dasuccess = dasuccess
        this.ulge = ulge
        this.eatl = eatl
        this.eatf = eatf
        this.eatn = eatn
        this.dap1spam = dap1spam
        this.af = af
        this.shutdown = shutdown
        // beautiful, ain't it?
        document.addEventListener(eventctrl.reinitSuccess.type, this.fadecontrol.bind(this, 500, this.le, this.suesucc))
        document.addEventListener(eventctrl.reinitFailure.type, this.fadecontrol.bind(this, 500, this.le, this.suefail))
        document.addEventListener(eventctrl.failedLoad.type, this.fadecontrol.bind(this, 500, this.fle, this.le))
        document.addEventListener(eventctrl.successLoad.type, this.fadecontrol.bind(this, 500, this.se, this.le))
        document.addEventListener(eventctrl.loginSuccess.type, this.fadecontrol.bind(this, 500, this.le, this.lge))
        document.addEventListener(eventctrl.initLoginFailure.type, this.fadecontrol.bind(this, 500, this.lge, this.le))
        document.addEventListener(eventctrl.makeReservations.type, this.fadecontrol.bind(this, 500, this.sue, this.se))
        document.addEventListener(eventctrl.makeReservationsFail.type, this.fadecontrol.bind(this, 500, this.suefail, this.sue))
        document.addEventListener(eventctrl.deleteReservationsInit.type, this.fadecontrol.bind(this, 500, this.dap1, this.se))
        document.addEventListener(eventctrl.deleteReservationsFail.type, this.fadecontrol.bind(this, 500, this.dafail, this.da))
        document.addEventListener(eventctrl.deleteReservationsFail2.type, this.fadecontrol.bind(this, 500, this.se, this.dafail))
        document.addEventListener(eventctrl.makeReservationsSuccess.type, this.fadecontrol.bind(this, 500, this.suesucc, this.sue))
        document.addEventListener(eventctrl.deleteReservationsP1None.type, this.fadecontrol.bind(this, 500, this.dap1none, this.dap1))
        document.addEventListener(eventctrl.deleteReservationsP1Fail.type, this.fadecontrol.bind(this, 500, this.dap1fail, this.dap1))
        document.addEventListener(eventctrl.deleteReservationsP1None2.type, this.fadecontrol.bind(this, 500, this.se, this.dap1none))
        document.addEventListener(eventctrl.deleteReservationsP1Fail2.type, this.fadecontrol.bind(this, 500, this.se, this.dap1fail))
        document.addEventListener(eventctrl.deleteReservationsSuccess.type, this.fadecontrol.bind(this, 500, this.dasuccess, this.da))
        document.addEventListener(eventctrl.deleteReservationsSuccess2.type, this.fadecontrol.bind(this, 500, this.se, this.dasuccess))
        document.addEventListener(eventctrl.deleteReservationsP1Success.type, this.fadecontrol.bind(this, 500, this.da, this.dap1))
        document.addEventListener(eventctrl.loginSwitchUserID.type, this.fadecontrolsync.bind(this, 500, this.ulge, this.lge))
        document.addEventListener(eventctrl.loginSwitchNormal.type, this.fadecontrolsync.bind(this, 500, this.lge, this.ulge))
        document.addEventListener(eventctrl.loginUserIDSuccess.type, this.fadecontrol.bind(this, 500, this.le, this.ulge))
        document.addEventListener(eventctrl.eatNowStart.type, this.fadecontrol.bind(this, 100, this.eatl, this.se))
        document.addEventListener(eventctrl.eatNowFailed.type, this.fadecontrol.bind(this, 500, this.eatf, this.eatl))
        document.addEventListener(eventctrl.eatNowNothing.type, this.fadecontrol.bind(this, 500, this.eatn, this.eatl))
        document.addEventListener(eventctrl.eatNowSuccess.type, this.fadecontrol.bind(this, 500, this.se, this.eatl))
        document.addEventListener(eventctrl.eatnowNothingReset.type, this.fadecontrol.bind(this, 500, this.se, this.eatn))
        document.addEventListener(eventctrl.eatNowFailedReset.type, this.fadecontrol.bind(this, 500, this.se, this.eatf))
        document.addEventListener(eventctrl.deleteReservationsP1SpamWarn.type, this.fadecontrol.bind(this, 500, this.dap1spam, this.dap1))
        document.addEventListener(eventctrl.deleteReservationsP1SpamDeny.type, this.fadecontrol.bind(this, 500, this.se, this.dap1spam))
        document.addEventListener(eventctrl.deleteReservationsP1SpamConfirm.type, this.fadecontrol.bind(this, 500, this.da, this.dap1spam))
        document.addEventListener(eventctrl.aprilFoolsFadeIn.type, this.fadecontrolsync.bind(this, 500, this.af, this.le))
        document.addEventListener(eventctrl.aprilFoolsFadeOut.type, this.fadecontrolsync.bind(this, 500, this.le, this.af))
        document.addEventListener(eventctrl.shutdownFade.type, this.fadecontrolsync.bind(this, 500, this.shutdown, this.le))

        consolelog("UIController", "UIController constructed.")
    }

    /**
     * Fades between two elements.
     * @param {*} duration The duration of the fade.
     * @param {*} ie The element to fade in.
     * @param {*} oe The element to fade out.
     */
    fadecontrol(duration, ie, oe) {
        consolelog("UIController/fadecontrol", "Fading async in " + $(ie).attr("id") + ", fading out " + $(oe).attr("id") + ", total duration of " + duration + "ms.")
        $(ie).fadeIn(duration)
        $(oe).fadeOut(duration)
    }

    fadecontrolsync(duration, ie, oe) {
        consolelog("UIController/fadecontrolsync", "Fading sync in " + $(ie).attr("id") + ", fading out " + $(oe).attr("id") + ", total duration of " + duration + "ms.")
        $(oe).fadeOut(duration / 2, function() {
            $(ie).fadeIn(duration / 2)
        })
    }

    /**
     * Controls if DOCBatch needs to switch to a failed loader, in the event of a problem.
     * @param {*} errorcode The error code of what happened.
     * @param {*} message An english explanation of the error.
     */
    failedload(errorcode, message) {
        consolelog("UIController/failedload", "Failed load fade.")
        this.fadecontrol(500, this.fle, this.le)
    }

    
}