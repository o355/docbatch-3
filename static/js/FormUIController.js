/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

/**
 * Mostly controls the UI for the labels
**/

class FormUIController {
    constructor(s1ctrl, s2ctrl, s3ctrl, s4ctrl, s5ctrl, stepper, reservations, locations, closedmodal) {
        this.s1ctrl = s1ctrl
        this.s2ctrl = s2ctrl
        this.s3ctrl = s3ctrl
        this.s4ctrl = s4ctrl
        this.s5ctrl = s5ctrl
        this.stepper = stepper
        this.reservations = reservations
        this.locations = locations
        this.closedmodal = closedmodal
        document.addEventListener(eventctrl.backToStart.type, this.backtostart.bind(this))
        consolelog("FormUIController", "A new FormUIController has been created.")
    }

    skips1tos5() {
        consolelog("FormUIController/skips1tos5", "Skipping from Step 1 to 5.")
        $("input[name=" + this.s1ctrl.formname + "]").prop("checked", true)
        $(this.stepper).nextStep()
        $(this.s2ctrl.input).val("1")
        $(this.stepper).nextStep()
        $(this.s3ctrl.input).val("1")
        $(this.stepper).nextStep()
        $(this.s4ctrl.timesinput).val("1")
        $("input[name=" + this.s4ctrl.downame).prop("checked", true)
        $(this.stepper).nextStep()
        $(this.s1ctrl.step).removeClass("done")
        $(this.s2ctrl.step).removeClass("done")
        $(this.s3ctrl.step).removeClass("done")
        $(this.s4ctrl.step).removeClass("done")
        this.disableall_excl5()
    }

    s1validate() {
        var result = this.s1ctrl.validate()
        if (result) {
            consolelog("FormUIController/s1validate", "S1 validated! Moving to next step in 10ms...")
            setTimeout(function() {
                $(this.stepper).nextStep()
                this.s1ctrl.updatelabel()
            }.bind(this), 10)
        } else {
            consolelog("FormUIController/s1validate", "S1 NOT validated. Not moving on to next step.")
            $(this.stepper).nextStep()
        }
        return false
    }

    reservefornow(bypassconfirm) {
        if ($("input[name=" + this.s1ctrl.formname + "]:checked").val() == undefined) {
            consolelog("FormUIController/reservefornow", "Failed to reserve for now, no location selected.")
            this.s1ctrl.validate()
            $(this.stepper).nextStep()
            return
        }
        var location = $("input[name=" + this.s1ctrl.formname + "]:checked").val()
        var statusdict = this.locations.getoperationstatus(location)
        if (statusdict['status'] == "closed" && bypassconfirm != true && statusdict['secondsleft'] > 900) {
            consolelog("FormUIController/reservefornow", "Location " + location + " is closed, bypass false, throwing modal.")
            $(this.closedmodal).modal("show")
            return
        }
        var timestamp = new Date().getTime() / 1000
        var adjusted = Math.round(timestamp / 1800)
        var finalts = adjusted * 1800
        if (statusdict['secondsleft'] <= 900 && statusdict['secondsleft'] != -1 && statusdict['status'] != "closed") {
            finalts = finalts - 1800
        }
        var momentobj = moment.unix(finalts)
        $(this.s2ctrl.input).val(momentobj.format("MM/DD/YYYY"))
        $(this.s3ctrl.input).val(momentobj.format("h:mm A"))
        $(this.s4ctrl.timesinput).val("1")
        consolelog("FormUIController/reservefornow", "Making reservation at " + location + " for " + momentobj.format("MM/DD/YYYY") + " " + momentobj.format("h:mm A"))
        $("input[name=" + this.s4ctrl.downame + "]").prop("checked", true)
        this.undisable_5()
        setTimeout(function() {
            $(this.stepper).nextStep()
            this.s1ctrl.updatelabel()
            $(this.stepper).nextStep()
            this.s2ctrl.updatelabel()
            $(this.stepper).nextStep()
            this.s3ctrl.updatelabel()
            $(this.stepper).nextStep()
            this.s4ctrl.updatelabel()
            this.reservations.createsinglereservation()
            setTimeout(function () {
                this.disableall_excl5()
            }.bind(this), 10)
            setTimeout(function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $(this.s5ctrl.step).offset().top
                }, 10)
            }.bind(this), 300)
        }.bind(this), 10)
    }

    // Validates Step 2, allowing it to move onto the next step. Could probably use jQuery Validator, but meh.
    s2validate(onclick) {
        var result = this.s2ctrl.validate(onclick)
        if (result) {
            consolelog("FormUIController/s2validate", "Validated Step 2! Moving to next step in 10ms.")
            setTimeout(function() {
                $(this.stepper).nextStep()
            }.bind(this), 10)
        }
        return true
    }

    // Updates the text in the Step 3 clock so it's not ugly.
    // Because the built-in callbacks don't work, this gets called every time the field is changed.
    

    // Mostly copy/paste code from above. Using the same technique to allow for proper validation.
    // Difference is in that there's an add 1 reservation or add reocurring, so we have to account for clicking on that button
    // and moving to S5, or just moving to the next step (S4)
    s3validate(onclick, skipsteps) {
        var result = this.s3ctrl.validate(onclick)
        if (result) {
            if (skipsteps) {
                consolelog("FormUIController/s3validate", "S3 is being validated with skipping steps.")
                setTimeout(function () {
                    $(this.s4ctrl.timesinput).val("1")
                    $("input[name=" + this.s4ctrl.downame + "]").prop("checked", true)
                    this.s4ctrl.updatelabel()
                    this.undisable_5()
                    setTimeout(function () {
                        $(this.stepper).nextStep()
                        $(this.stepper).nextStep()
                        this.reservations.createsinglereservation()
                        this.disableall_excl5()
                        setTimeout(function () {
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $(this.s5ctrl.step).offset().top
                            }, 10)
                        }.bind(this), 300)
                    }.bind(this), 10)
                }.bind(this), 10)
            } else {
                consolelog("FormUIController/s3validate", "S3 is being validated without skipping steps.")
                if (onclick != true) {
                    setTimeout(function () {
                        $(this.stepper).nextStep()
                    }.bind(this), 10)
                }
            }
        }
        return false
    }

    s4addbutton() {
        if (parseInt($(this.s4ctrl.timesinput).val()) < parseInt($(this.s4ctrl.timesinput).attr("min")) || parseInt($(this.s4ctrl.timesinput).val()) > parseInt($(this.s4ctrl.timesinput).attr("max"))) {
            consolelog("FormUIController/s4addbutton", "Reservation times input out of range to continue.")
            return
        }

        if ($('input[name="' + this.s4ctrl.downame + '"]:checked').length < 1) {
            consolelog("FormUIController/s4addbutton", "Reservation days checked are none. Not continuing.")
            $(this.stepper).nextStep()
            return
        }

        consolelog("FormUIController/s4addbutton", "S4 validated. Moving on to S5.")
        this.s4ctrl.updatelabel()
        this.undisable_5()
        setTimeout(function () {
            $(this.stepper).nextStep()
            this.reservations.createrecursivereservation()
            this.disableall_excl5()
            setTimeout(function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $(this.s5ctrl.step).offset().top
                }, 10)
            }.bind(this), 300)
        }.bind(this), 10)
    }

    s4onclick() {
        consolelog("FormUIController/s4onclick", "Called!")
        this.s4ctrl.updatelabel()
        this.reservations.createrecursivereservation()
        this.disableall_excl5()
    }

    undisable_5() {
        consolelog("FormUIController/undisable_5", "Undisabling S5")
        this.s5ctrl.disable(false)
        $(this.s5step).removeClass("disabled")
    }

    disableall_excl5() {
        consolelog("FormUIController/disableall_excl5", "Disabling all except S5")
        this.s1ctrl.disable(true)
        this.s2ctrl.disable(true)
        this.s3ctrl.disable(true)
        this.s4ctrl.disable(true)
        this.s5ctrl.disable(false)
    }

    undisableall() {
        consolelog("FormUIController/undisableall", "Undisabling all!")
        this.s1ctrl.disable(false)
        this.s2ctrl.disable(false)
        this.s3ctrl.disable(false)
        this.s4ctrl.disable(false)
        this.s5ctrl.disable(false)
    }

    disable_5() {
        consolelog("FormUIController/disable_5", "Disabling Step 5")
        this.s5ctrl.disable(true)
    }

    clearlabels() {
        consolelog("FormUIController/clearlabels", "Clearing all labels!")
        this.s1ctrl.updatelabel("")
        this.s2ctrl.updatelabel("")
        this.s3ctrl.updatelabel("")
        this.s4ctrl.updatelabel("")
        $(this.s2ctrl.step).removeClass("done")
        $(this.s3ctrl.step).removeClass("done")
        $(this.s4ctrl.step).removeClass("done")
        $(this.s5ctrl.step).removeClass("done")
    }

    clearinputs() {
        consolelog("FormUIController/clearinputs", "Clearing all inputs!")
        $("input[name=" + this.s1ctrl.formname + "]").prop("checked", false)
        $(this.s2ctrl.input).val("")
        $(this.s3ctrl.input).val("")
        $(this.s4ctrl.timesinput).val("1")
        $("input[name=" + this.s4ctrl.downame).prop("checked", true)
    }

    backtostart() {
        consolelog("FormUIController/backtostart", "Going back to start!")
        this.undisableall()
        setTimeout(function () {
            $(this.stepper).nextStep()
            $(this.stepper).nextStep()
            this.clearlabels()
            this.clearinputs()
            this.disable_5()
            setTimeout(function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $(this.s1ctrl.step).offset().top
                }, 10)
            }.bind(this), 300)
        }.bind(this), 10)

    }
}