/* 
    DOCBatch 3
    Copyright (C) 2021  Owen McGinley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

/**
 * Wraps an XHR response object for our needs.
 */
// eslint-disable-next-line no-unused-vars
class XHRwrap {
    constructor (url, method, async, timeout, json, callback) {
        this.url = url
        this.method = method
        this.async = async
        this.timeout = timeout
        this.json = json
        this.callback = callback
        consolelog("XHRwrap", "Constructing new XHRwrap to go to " + url)
    }

    request() {
        consolelog("XHRwrap/request", "Request is now going through!")
        var request = new XMLHttpRequest()
        request.open(this.method, this.url, this.async)
        request.timeout = this.timeout
        if (this.json != null) {
            request.setRequestHeader("Content-Type", "application/json")
            request.send(JSON.stringify(this.json))
        } else {
            request.send()
        }

        request.onload = this.callback.bind(this, request)
        request.onerror = this.callback.bind(this, request)
        request.ontimeout = this.callback.bind(this, request)
    }
}